function [pulses,time_delays]=pulse_alignment(subj,tmask,pulse_list,pulse_count,roi_time,TR,plot_timecourses)

% extract pulse time courses
lead=12*1.1/TR; % so the different TRs cover the same amount of time
lag=16*1.1/TR;
num_rois=size(roi_time,1);

pulses=zeros(num_rois,1+lead+lag, sum(pulse_count));
k=1;
for i=1:size(roi_time,3)
    roi_time(:,~tmask(i,:),i)=NaN;
    if pulse_count(i)>0
        time=[NaN(num_rois,lead) roi_time(:,:,i) NaN(num_rois,lag)];
        for j=1:length(pulse_list{i})
            x=pulse_list{i}(j);
            pulses(:,:,k)=time(:,x:x+lead+lag);
            k=k+1;
        end
    end
end
pulses=pulses/10;

% align pulses by parabolic interpolation of cross-correlation
roi1=1; % L-SM1 (1 = L-SM1, 2 = R-SM1, 3/4 = L/R-SMA, 5/6 = L/R-Insula, 7/8 = L/R-Cblm (superior), 9/10 = L/R-Cblm (inf), 11/12 = L/R-Thal, 13/14 = L/R-Put)
roi1=roi1+14; % Hand (0 = Tongue, 14 = Hand, 28 = Foot)
rois=[3 1 8]+14;
roi_names={'SMA','SM1','Cblm'};
reference_roi=2;

m=nanmean(pulses(roi1,:,:),3);
duration = length(m);

if(plot_timecourses)
    figure('position',[100 100 1000 1000]); hold on
    %title('parabolic interp')
end

num_pulses=size(pulses,3);
time_delays=nan(num_rois,num_pulses);
%good_pulses=[];
for pulse=1:num_pulses
    for roi=rois
        y = pulses(roi,:,pulse);
        
        % find delay
        max_lag=4*1.1/TR;
        cc=xcorr(y,m,max_lag);
        [~,l]=max(cc);
        if (l<length(cc) && l>1)
            td=parabolic_interp(cc(l-1:l+1));
            td=td+l-ceil(length(cc)/2);
        else
            td=NaN;
        end
        
        % find goodness of fit along peak of pulse
        %result=shift_time(m,td)';
        %mid_y=y(1,middle_range);
        %result=result(middle_range);
        %qc=corr(result(~isnan(mid_y)),mid_y(~isnan(mid_y))');

        time_delays(roi,pulse)=td*TR;
        
        if (roi==roi1) % && qc>.95)
            if (plot_timecourses)
                plot(((1:duration)-td)*TR,pulses(roi1,:,pulse),'color',[0 0 0 .5])
            end
%             good_pulses=[good_pulses pulse];
        end
    end
end

%
assert(length(rois)==length(roi_names),'Error: #rois != #roi_names')

% plot relative pulse times
figure; hold on
plot([0 0],[.5 length(rois)+0.5],'--','color','k')
%paths=time_delays(rois,good_pulses);
paths=time_delays(rois,:);
n=sum(isnan(paths),1)>0;
paths=paths(:,~n);
paths=paths-repmat(paths(reference_roi,:),length(rois),1);
x=repmat((length(rois):-1:1)',1,sum(~n));
s=scatter(paths(:),x(:)+0.02*randn(size(x(:))),10,[0 0 0],'filled');
s.MarkerFaceAlpha=0.5;

e=std(paths')/sqrt(sum(~n));
m=median(paths');
h=errorbar(m,(length(rois):-1:1),e,'horizontal','color',[0 .5 1],'linewidth',2);
h.CapSize=8;

axis([-4 4 .5 length(rois)+0.5])
set(gca,'ytick',1:length(rois))
set(gca,'yticklabel',flip(roi_names))
%title(subj)

% print stats
disp(subj)
for i=1:length(rois)-1
    for j=i+1:length(rois)
        diff=median(paths(j,:)-paths(i,:));
        p=signrank(paths(j,:)-paths(i,:));
        disp([roi_names{i} ' --> ' roi_names{j} ': ' num2str(diff*1000) 'ms (p = ' num2str(p) ')'])
    end
end

