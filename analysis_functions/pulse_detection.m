function [pulse_list, pulse_count] = pulse_detection(tmask_spread, tmask, roi_time, roi1, roi2, thresh1, thresh2, plot_pulse_locations, plot_pulse_counts, file_suffix, cast_scannums, cast_color)

% apply tmask + spread if needed
for i=1:size(roi_time,3)
    roi_time(:,~tmask(i,:),i)=nan;
    if (tmask_spread)
        pad=nan(size(roi_time,1), tmask_spread);
        padded_roi_time=[pad roi_time(:,:,i) pad];
        for j=1:tmask_spread
            padded_roi_time(:,find(~tmask(i,:))+j+tmask_spread)=nan;
            padded_roi_time(:,find(~tmask(i,:))-j+tmask_spread)=nan;
        end
        roi_time(:,:,i)=padded_roi_time(:,tmask_spread+1:end-tmask_spread);
    end
end

if plot_pulse_locations
    spacing=20;
    h=figure('position',[0 0 500 1000]);
    hold on
end

clear pulse_count pulse_list
for i=1:size(roi_time,3)
    if plot_pulse_locations
        plot(TR:TR:TR*size(roi_time,2),roi_time(roi1,:,i)-i*spacing,'b');
        plot(TR:TR:TR*size(roi_time,2),roi_time(roi2,:,i)-i*spacing,'r');
    end
    [~,x]=findpeaks(roi_time(roi1,:,i));
    if ~isempty(x)
        x=x(roi_time(roi1,x,i)'> thresh1 & (roi_time(roi1,x,i)'-roi_time(roi2,x,i)')>thresh2);
        if length(x)>1
            %pick out peaks that occur within 10 seconds of each other
            while true
                dx=diff(x);
                doubles=find([dx<10 logical(0)]);
                if isempty(doubles)
                    break
                else
                    if(roi_time(1,x(doubles(1)),i)>roi_time(1,x(doubles(1)+1),i))
                        %first peak higher --> get rid of second
                        x(doubles(1)+1)=[];
                    else
                        %second peak higher --> get rid of first
                        x(doubles(1))=[];
                    end
                end
            end
        end
    end
    pulse_count(i)=length(x);
    pulse_list{i}=x;
    if plot_pulse_locations
        scatter(x*TR,roi_time(roi1,x,i)-i*spacing+1,'+k');
    end
end

%save([detection_outdir subject '_pulse_list' file_suffix '.mat'],'pulse_list','pulse_count')

if plot_pulse_locations
    xlabel('Time (s)')
    ylabel('Session')
    set(gca,'ytick',-floor(length(pulse_count)/5)*5*spacing:spacing*5:-spacing*5)
    set(gca,'yticklabel',floor(length(pulse_count)/5)*5:-5:5)
    saveas(h,[detection_outdir subject '_pulse_locations' file_suffix '.eps'],'epsc')
    saveas(h,[detection_outdir subject '_pulse_locations' file_suffix '.fig'])
end

if plot_pulse_counts
  h=figure('position',[400 400 300 200]); hold on
  area([min(cast_scannums)-.5 max(cast_scannums)+.5], [40 40], 0, 'FaceColor', cast_color, 'LineStyle', 'none');
  scatter(1:length(pulse_count),pulse_count,'k')
  xlabel('Session')
  ylabel('Pulse count')
  %saveas(h,[detection_outdir subject '_pulse_count' file_suffix '.eps'],'epsc')
end
