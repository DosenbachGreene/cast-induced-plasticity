function [lu,ru,N]=accel_use_counts_DJN(acceldir)


sleep_thresh=100;
window=900;

% load data
load([acceldir 'LHand.mat'],'data','starts')
ldata=data;
starttime=starts(4,1)*3600+starts(5,1)*60+starts(6,1);
ldata=[NaN(1,starttime) ldata NaN(1,24*3600)];
load([acceldir 'RHand.mat'],'data','starts')
rdata=data;
starttime=starts(4,1)*3600+starts(5,1)*60+starts(6,1);
rdata=[NaN(1,starttime) rdata NaN(1,24*3600)];

for i=1:floor(length(ldata)/3600/24)
    daytime=24*3600*(i-1)+1:24*3600*i;
    day=ldata(daytime);
    sleep=NaN(1,length(day));
    for j=1:length(day)/window-1
        if(sum(day((j-1)*window+1:j*window)>10)<sleep_thresh)
            sleep((j-1)*window+1:j*window)=1;
        end
    end
    sleep(isnan(day))=NaN;    
    rday=rdata(daytime);
    lday=ldata(daytime);
    n=sum(~isnan(rday(isnan(sleep))));
    if n>length(daytime)*.2
        ru(i)=sum(rday(isnan(sleep))>10)/n;
        lu(i)=sum(lday(isnan(sleep))>10)/n;
    else
        ru(i)=NaN;
        lu(i)=NaN;
    end
    N(i)=n;
end

end