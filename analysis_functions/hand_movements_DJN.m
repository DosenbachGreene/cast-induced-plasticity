hand_movements_DJN(movement_thresh, amplitude_change, movedir, bold_list, pulse_list, pulse_count, roi_time, tmask, cast_color)

resp_list=importdata([movedir 'matlist_TEST.txt']);

for i=1:length(resp_list)
    resp_file=resp_list{i};
    padding=20000;
    bandpass_params = [1 30]; % Hz
    sampling_rate = [400]; % Hz
    %movement_z_thresh=0.01;
    movement_thresh = 10;
    thresh_smooth = 1; % second

    %%
    load(resp_file)
    resp.data=double(resp.data);
    padded=[resp.data(1)*ones(padding,1); resp.data; resp.data(end)*ones(padding,1)];
    bp=bandpass(padded,[bandpass_params],sampling_rate);
    bp=bp(padding+1:end-padding);
    bp_abs=abs(bp);

    %%
    resp.data=resp.data-mean(resp.data);
    resp.data=detrend(resp.data);
    [P1,f]=fft_dn(resp.data,1/400);
    figure;
    subplot(4,1,1:2);
    plot(f,smooth(P1,2,'lowess'));
    axis([0 1 0 5000000]) %mean(P1)+0.5*std(P1)])
    subplot(4,1,3:4);
    [P1,f]=fft_dn(bp,1/400);
    plot(f,smooth(P1,10));
    axis([0 max(f) 0 50000]) %0.5*std(P1)])
    %%
    %movement_thresh=m+s*movement_z_thresh;
    thresh=abs(bp)>movement_thresh;
    thresh=smooth(thresh,sampling_rate*thresh_smooth)>0;
    thresh(1)=0;

    % find continuous segments of elevated signal
    peak=0;
    starts=[];
    stops=[];
    for i=2:length(thresh)
        if (thresh(i) && ~thresh(i-1))
            peak=peak+1;
            starts(peak)=i;
        elseif (thresh(i-1) && ~thresh(i))
            stops(peak)=i;
        end
    end
    if (length(stops) < peak)
        stops(peak)=length(thresh);
    end
    % find peak signal in each segment
    hand_movements=zeros(1,peak);
    for i=1:peak
        [~,x]=max(bp(starts(i):stops(i)));
        hand_movements(i)=x+starts(i);
    end
    peaks=zeros(size(thresh));
    peaks(hand_movements)=1;
    % convert to seconds from the first signal
    hand_movements=(hand_movements-double(resp.slicetimes(1)))/400;
    %
    figure;
    subplot(6,1,1:2)
    x=double(resp.data);
    m=mean(x);
    s=std(x);
    plot(x)
    axis([0 length(x) m-300 m+300])
    %axis([0 length(x) m-s m+s])

    % plot bp + movement_thresh
    x=bp;
    s=std(x);
    subplot(6,1,3:4)
    hold on
    area([0 length(x)],[1 1]*movement_thresh,-movement_thresh,'facecolor',[.7 .7 .7],'edgecolor','none','showbaseline','off')
    plot(x,'k')
    axis([0 length(x) -150 +150])
    %axis([0 length(x) -2*s +2*s])

    subplot(6,1,5)
    imagesc(thresh')

    subplot(6,1,6);
    plot(peaks)
    %set(gca,'xtick',resp.slicetimes(1:40:end))
    %set(gca,'xticklabel',0:floor(40*1.1):end)
    axis([0 length(x) 0 1])

end