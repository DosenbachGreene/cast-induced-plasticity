function grow_rois_DJN(subject, task_dir, fsdir, outdir, labeltype)

roi_sizes=[400 40 10]; % cortex (vertices), cerebellum (voxels), putamen (voxels)

representation={'Hand', 'Foot', 'Tongue'};
task_conditions={'LHand' 'RHand' 'LFoot' 'RFoot' 'Tongue'};
relevant_conditions={[1 2],[3 4],[5 5]};
switch labeltype
    case 'old'
        roi_labels = {[3 13 31],[27 29 33]};
    case 'new'
        roi_labels = {[16 21 23],[22 24 27]};
end

for z=1:3
    disp(representation{z});
    % load peak, task and surface files
    peaks=ft_read_cifti_mod([outdir subject '_' representation{z} '_peaks.dtseries.nii']);
    task=ft_read_cifti_mod([task_dir 'sync/' subject '_' task_conditions{relevant_conditions{z}(2)} '_sync.dtseries.nii']);
    lsurf=gifti([fsdir subject '.L.midthickness.32k_fs_LR.surf.gii']);
    rsurf=gifti([fsdir subject '.R.midthickness.32k_fs_LR.surf.gii']);
    atlasl=gifti([fsdir subject '.L.aparc.32k_fs_LR.label.gii']);
    atlasr=gifti([fsdir subject '.R.aparc.32k_fs_LR.label.gii']);
    lsurf_size=length(lsurf.vertices);
    rsurf_size=length(rsurf.vertices);
    lsurf_mask=task.brainstructure(1:lsurf_size)>0;
    rsurf_mask=task.brainstructure(lsurf_size+1:lsurf_size+rsurf_size)>0;

    % pull cortical data out of task file
    tasklsurf=zeros(lsurf_size,1);
    tasklsurf(lsurf_mask)=task.data(1:sum(lsurf_mask));
    peakslsurf=zeros(lsurf_size,1);
    peakslsurf(lsurf_mask)=peaks.data(1:sum(lsurf_mask));
    peaksrsurf=zeros(rsurf_size,1);
    peaksrsurf(rsurf_mask)=peaks.data(sum(lsurf_mask)+1:sum(lsurf_mask)+sum(rsurf_mask));
    grown_rois=task;
    grown_rois.data=zeros(size(grown_rois.data));
    lsurf_rois=zeros(lsurf_size,1);
    
    % Grow ROI in L-SM1
    roi_verts=find(peakslsurf==1);
    region_verts=find(ismember(atlasr.cdata,roi_labels{1}));
    for j=1:roi_sizes(1)-1
        neighbors=unique(lsurf.faces(sum(ismember(lsurf.faces,roi_verts),2)>0,:));
        neighbors(ismember(neighbors,roi_verts))=[];
        neighbors(~ismember(neighbors,region_verts))=[];
        [~,maxi]=max(tasklsurf(neighbors));
        roi_verts(j+1)=neighbors(maxi);
    end
    lsurf_rois(roi_verts)=1;
    
    % Grow ROI in L-SMA
    roi_verts=find(peakslsurf==1);
    region_verts=find(ismember(atlasr.cdata,roi_labels{2}));
    for j=1:roi_sizes(1)-1
        neighbors=unique(lsurf.faces(sum(ismember(lsurf.faces,roi_verts),2)>0,:));
        neighbors(ismember(neighbors,roi_verts))=[];
        neighbors(~ismember(neighbors,region_verts))=[];
        [~,maxi]=max(tasklsurf(neighbors));
        roi_verts(j+1)=neighbors(maxi);
    end
    lsurf_rois(roi_verts)=3;
    grown_rois.data(grown_rois.brainstructure(grown_rois.brainstructure>0)==1)=lsurf_rois(lsurf_mask);
    
    % pull positions and brain labels out of cifti
    vol_positions=peaks.pos(peaks.brainstructure>0,:);
    structures=task.brainstructure(task.brainstructure>0);
    pos=task.pos(task.brainstructure>0,:);
    
    % grow ROI in R-Cblm
    roi_ind=find(structures==find(strcmp(task.brainstructurelabel,'CEREBELLUM_RIGHT')));
    roi_pos=pos(roi_ind,:);
    midpoint=mean([max(roi_pos(:,3)) min(roi_pos(:,3))]);
    roi_ind=roi_ind(roi_pos(:,3)>=midpoint); % only grow in superior half
    struct_voxels=pos(roi_ind,:);
    struct_task=(task.data(roi_ind));
    roi_voxels=vol_positions(peaks.data==4,:);
    neighbors=[];
    for j=1:roi_sizes(2)-1
        neighbors=[neighbors; repmat(roi_voxels(j,:),6,1)+[-1 0 0; 1 0 0; 0 -1 0; 0 1 0; 0 0 -1; 0 0 1]*3];
        neighbors(ismember(neighbors,roi_voxels,'rows'),:)=[];
        neighbors=neighbors(ismember(neighbors,struct_voxels,'rows'),:);
        if(isempty(neighbors))
            break
        end
        neighbor_values=zeros(size(neighbors,1),1);
        for l=1:size(neighbors,1)
            neighbor_values(l)=struct_task(ismember(struct_voxels,neighbors(l,:),'rows'));
        end
        [~,maxi]=max(neighbor_values);
        roi_voxels(j+1,:)=neighbors(maxi,:);
    end
    grown_rois.data(ismember(vol_positions,roi_voxels,'rows'))=6;
    
        
    task=ft_read_cifti_mod([task_dir 'sync/' subject '_' task_conditions{relevant_conditions{z}(1)} '_sync.dtseries.nii']);
    taskrsurf=zeros(rsurf_size,1);
    taskrsurf(rsurf_mask)=task.data((sum(lsurf_mask)+1):(sum(lsurf_mask)+sum(rsurf_mask)));
    rsurf_rois=zeros(rsurf_size,1);
    
    % Grow ROI in R-SM1
    roi_verts=find(peaksrsurf==2);
    region_verts=find(ismember(atlasr.cdata,roi_labels{1}));
    for j=1:roi_sizes(1)-1
        neighbors=unique(rsurf.faces(sum(ismember(rsurf.faces,roi_verts),2)>0,:));
        neighbors(ismember(neighbors,roi_verts))=[];
        neighbors(~ismember(neighbors,region_verts))=[];
        [~,maxi]=max(taskrsurf(neighbors));
        roi_verts(j+1)=neighbors(maxi);
    end
    rsurf_rois(roi_verts)=2;
    
    % Grow ROI in R-SMA
    roi_verts=find(peaksrsurf==2);
    region_verts=find(ismember(atlasr.cdata,roi_labels{2}));
    for j=1:roi_sizes(1)-1
        neighbors=unique(rsurf.faces(sum(ismember(rsurf.faces,roi_verts),2)>0,:));
        neighbors(ismember(neighbors,roi_verts))=[];
        neighbors(~ismember(neighbors,region_verts))=[];
        [~,maxi]=max(taskrsurf(neighbors));
        roi_verts(j+1)=neighbors(maxi);
    end
    rsurf_rois(roi_verts)=4;
    grown_rois.data(grown_rois.brainstructure(grown_rois.brainstructure>0)==2)=rsurf_rois(rsurf_mask);
    
    % pull positions and brain labels out of cifti
    vol_positions=peaks.pos(peaks.brainstructure>0,:);
    structures=task.brainstructure(task.brainstructure>0);
    pos=task.pos(task.brainstructure>0,:);
    
    % grow ROI in L-Cblm
    roi_ind=find(structures==find(strcmp(task.brainstructurelabel,'CEREBELLUM_LEFT')));
    roi_pos=pos(roi_ind,:);
    midpoint=mean([max(roi_pos(:,3)) min(roi_pos(:,3))]);
    roi_ind=roi_ind(roi_pos(:,3)>=midpoint); % only grow in superior half
    struct_voxels=pos(roi_ind,:);
    struct_task=(task.data(roi_ind));
    roi_voxels=vol_positions(peaks.data==3,:);
    neighbors=[];
    for j=1:roi_sizes(2)-1
        neighbors=[neighbors; repmat(roi_voxels(j,:),6,1)+[-1 0 0; 1 0 0; 0 -1 0; 0 1 0; 0 0 -1; 0 0 1]*3];
        neighbors(ismember(neighbors,roi_voxels,'rows'),:)=[];
        neighbors=neighbors(ismember(neighbors,struct_voxels,'rows'),:);
        if(isempty(neighbors))
            break
        end
        neighbor_values=zeros(size(neighbors,1),1);
        for l=1:size(neighbors,1)
            neighbor_values(l)=struct_task(ismember(struct_voxels,neighbors(l,:),'rows'));
        end
        [~,maxi]=max(neighbor_values);
        roi_voxels(j+1,:)=neighbors(maxi,:);
    end
    grown_rois.data(ismember(vol_positions,roi_voxels,'rows'))=5;
    
    ft_write_cifti_mod([outdir subject '_' representation{z} '_ROIs'],grown_rois);
end

end