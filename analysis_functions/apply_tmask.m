function masked_time = apply_tmask(roi_time,tmask)

masked_time=roi_time;

for i=1:size(roi_time,3)
    for j=1:size(roi_time,1)
        masked_time(j,~tmask(i,:),i)=NaN;
    end
end