function SIC_primary_somatomotor_parcels_part2(subject,communities,parcel_path,fsdir,labeltype,outdir,problem_parcels,sort_parcels)
    
% load parcels
parcels=ft_read_cifti_mod(parcel_path);
parcel_list=unique(parcels.data(parcels.data>0));
struct=parcels.brainstructure(parcels.brainstructure>0);
for i=1:length(parcel_list);
   hem_list(i)=mode(struct(parcels.data==parcel_list(i)));
end
[~,sorti]=sort(hem_list);
if sort_parcels
    parcel_list=parcel_list(sorti);
end

% find hemispheres
struct=parcels.brainstructure(parcels.brainstructure>0);
assert(length(parcels.data)==length(struct));
hems=zeros(size(parcel_list));
for i=1:length(parcel_list)
    hems(i)=mode(struct(parcels.data==parcel_list(i)));
end

% import labels
label=gifti([fsdir subject '.L.aparc.32k_fs_LR.label.gii']);
lstruct=parcels.brainstructure(1:length(label.cdata));
llabel=label.cdata(lstruct==1);
label=gifti([fsdir subject '.L.aparc.32k_fs_LR.label.gii']);
rstruct=parcels.brainstructure((length(label.cdata)+1):(length(label.cdata)*2));
rlabel=label.cdata(rstruct==2);
lrlabel=[llabel; rlabel];

cortical_parcels=parcels.data(1:length(lrlabel));
assert(length(cortical_parcels)==length(lrlabel))
labels=zeros(size(parcel_list));
for i=1:length(labels)
    labels(i)=mode(lrlabel(cortical_parcels==parcel_list(i)));
end

switch labeltype
    case 'old'
        foot_label = 13;
        tongue_hand_labels = [3 31];
    case 'new'
        foot_label = 16;
        tongue_hand_labels = [21 23];
end

right_hand_parcels=parcel_list(communities==10 & hems==1 & ismember(labels,tongue_hand_labels));
left_hand_parcels=parcel_list(communities==10 & hems==2 & ismember(labels,tongue_hand_labels));
right_tongue_parcels=parcel_list(communities==11 & hems==1 & ismember(labels,tongue_hand_labels));
left_tongue_parcels=parcel_list(communities==11 & hems==2 & ismember(labels,tongue_hand_labels));
right_foot_parcels=parcel_list(communities==17 & hems==1 & ismember(labels, [tongue_hand_labels foot_label]));
left_foot_parcels=parcel_list(communities==17 & hems==2 & ismember(labels, [tongue_hand_labels foot_label]));
CON_parcels=parcel_list(communities==9);

output=parcels;
output.data=zeros(length(output.data),6);
for i=1:length(right_hand_parcels)
    output.data(parcels.data==right_hand_parcels(i),1)=i;
end
for i=1:length(left_hand_parcels)
    output.data(parcels.data==left_hand_parcels(i),2)=i;
end
for i=1:length(right_tongue_parcels)
    output.data(parcels.data==right_tongue_parcels(i),3)=i;
end
for i=1:length(left_tongue_parcels)
    output.data(parcels.data==left_tongue_parcels(i),4)=i;
end
for i=1:length(right_foot_parcels)
    output.data(parcels.data==right_foot_parcels(i),5)=i;
end
for i=1:length(left_foot_parcels)
    output.data(parcels.data==left_foot_parcels(i),6)=i;
end


% manual editing to remove extra parcels
right_hand_parcels(problem_parcels{1})=[];
left_hand_parcels(problem_parcels{2})=[];
right_tongue_parcels(problem_parcels{3})=[];
left_tongue_parcels(problem_parcels{4})=[];
right_foot_parcels(problem_parcels{5})=[];
left_foot_parcels(problem_parcels{6})=[];

output=parcels;
output.data=zeros(size(output.data));
output.data(ismember(parcels.data, right_hand_parcels))=1;
output.data(ismember(parcels.data, left_hand_parcels))=2;
output.data(ismember(parcels.data, right_foot_parcels))=3;
output.data(ismember(parcels.data, left_foot_parcels))=4;
output.data(ismember(parcels.data, right_tongue_parcels))=5;
output.data(ismember(parcels.data, left_tongue_parcels))=6;

ft_write_cifti_mod([outdir subject '_primary_somatomotor_parcels.dtseries.nii'],output)

right_hand_color=[0 1 1];
left_hand_color=[0 127 178]/255;
tongue_color=[1 0.5 0];
foot_color=[.13 .55 .13];
make_cifti_label([outdir subject '_primary_somatomotor_parcels.dtseries.nii'],[right_hand_color; left_hand_color; foot_color; foot_color; tongue_color; tongue_color])

% add CON parcels
output.data(ismember(parcels.data, CON_parcels))=7;
ft_write_cifti_mod([outdir subject '_primary_somatomotor_parcels_plusCON.dtseries.nii'],output)
CON_color=[.5 0 .5];
make_cifti_label([outdir subject '_primary_somatomotor_parcels_plusCON.dtseries.nii'],[right_hand_color; left_hand_color; foot_color; foot_color; tongue_color; tongue_color; CON_color])


end