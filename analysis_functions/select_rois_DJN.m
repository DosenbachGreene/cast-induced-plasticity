function select_rois_DJN(subject,task_dir,fsdir,outdir,labeltype)

representation={'Hand', 'Foot', 'Tongue'};
right_task_conditions={'RHand' 'RFoot' 'Tongue'};
left_task_conditions={'LHand' 'LFoot' 'Tongue'};

% deal with different versions of freesurfer output
switch labeltype
    case 'old'
        roi_labels = {[3 13 31],[27 29 33]};
    case 'new'
        roi_labels = {[16 21 23],[22 24 27]};
end

if (exist(outdir,'dir')~=7)
    system(['mkdir ' outdir])
end

peak_matrix_locations=zeros(6,3);
% load anatomical segmentation files
atlasl=gifti([fsdir subject '.L.aparc.32k_fs_LR.label.gii']);
atlasr=gifti([fsdir subject '.R.aparc.32k_fs_LR.label.gii']);

for j=1:3
    disp(representation{j});
    
    % load task data
    task=ft_read_cifti_mod([task_dir subject '_' right_task_conditions{j} '_sync.dtseries.nii']);
    % pull structure labels and position information out of cifti file
    structures=task.brainstructure(task.brainstructure>0);
    pos=task.pos(task.brainstructure>0,:);
    
    % find peak activation in L-SM1
    lroi=ismember(atlasl.cdata, roi_labels{1});
    lroi=lroi(task.brainstructure(1:length(atlasl.cdata))>0);
    roi_task=task.data(lroi);
    peak_location=find(task.data==max(roi_task(roi_task<inf)));
    peak_matrix_locations(1,j)=peak_location;
    
    % find peak activation in L-SMA
    rroi=ismember(atlasr.cdata, roi_labels{2});
    rroi=rroi(task.brainstructure(length(atlasl.cdata)+1:length(atlasl.cdata)+length(atlasr.cdata))>0);
    roi_task=task.data(logical([zeros(size(lroi)); rroi]));
    peak_location=find(task.data==max(roi_task(roi_task<inf)));
    peak_matrix_locations(3,j)=peak_location;
    
    % find peak activation in R-Cblm
    roi_ind=find(structures==find(strcmp(task.brainstructurelabel,'CEREBELLUM_RIGHT')));
    roi_pos=pos(roi_ind,:);
    midpoint=mean([max(roi_pos(:,3)) min(roi_pos(:,3))]);
    roi_ind=roi_ind(roi_pos(:,3)>=midpoint); % only serch the superior half of the cerebellum
    roi_task=task.data(roi_ind);
    peak_location=find(task.data==max(roi_task(roi_task<inf)));
    peak_matrix_locations(6,j)=peak_location;
    
    % load task data
    task=ft_read_cifti_mod([task_dir subject '_' left_task_conditions{j} '_sync.dtseries.nii']);
    % pull structure labels and position information out of cifti file
    structures=task.brainstructure(task.brainstructure>0);
    pos=task.pos(task.brainstructure>0,:);
    
    % find peak activation in R-SM1
    rroi=ismember(atlasr.cdata, roi_labels{1});
    rroi=rroi(task.brainstructure(length(atlasl.cdata)+1:length(atlasl.cdata)+length(atlasr.cdata))>0);
    roi_task=task.data(logical([zeros(size(lroi)); rroi]));
    peak_location=find(task.data==max(roi_task(roi_task<inf)));
    peak_matrix_locations(2,j)=peak_location;
    
    % find peak activation in R-SMA
    rroi=ismember(atlasr.cdata, roi_labels{2});
    rroi=rroi(task.brainstructure(length(atlasl.cdata)+1:length(atlasl.cdata)+length(atlasr.cdata))>0);
    roi_task=task.data(logical([zeros(size(lroi)); rroi]));
    peak_location=find(task.data==max(roi_task(roi_task<inf)));
    peak_matrix_locations(4,j)=peak_location;
    
    % find peak activation in L-Cblm
    roi_ind=find(structures==find(strcmp(task.brainstructurelabel,'CEREBELLUM_LEFT')));
    roi_pos=pos(roi_ind,:);
    midpoint=mean([max(roi_pos(:,3)) min(roi_pos(:,3))]);
    roi_ind=roi_ind(roi_pos(:,3)>=midpoint); % only search the superior half of the cerebellum
    roi_task=task.data(roi_ind);
    peak_location=find(task.data==max(roi_task(roi_task<inf)));
    peak_matrix_locations(5,j)=peak_location;
end


for z=1:3
    peak_file=task;
    peak_file.data=zeros(size(peak_file.data));
    for k=1:6
        peak_file.data(peak_matrix_locations(k,z))=k;
    end
    ft_write_cifti_mod([outdir subject '_' representation{z} '_peaks'],peak_file);
end
