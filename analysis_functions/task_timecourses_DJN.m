function task_timecourses_DJN(subject,TR,BIDS_dir,outdir)
% generates 3D matrix (vertices x time (30s following start cue) x trial)

disp(['task_timecourses_SIC: ' subject])

% read session list
trial_types=['Tongue'; 'LHand '; 'RHand '; 'LFoot '; 'RFoot '];
sessions=tdfread([BIDS_dir subject '/' subject '_sessions.tsv']);
sessions=sessions.SESSION;
num_runs=length(sessions);
onset=[];
trials=[];
k=0;
disp('reading event files...')
for i=1:num_runs
    for j=1:2
        file_name=[subject '_' sessions(i,:) '_task-motor_run-' num2str(j) '_events.tsv'];
        if(exist([BIDS_dir subject '/' sessions(i,:) '/func/' file_name],'file')==2)
            k=k+1;
            events=tdfread([BIDS_dir sessions(i,:) '/func/' file_name]);
            onset(:,k)=int32(events.onset/TR);
            [~, trials(:,k)]=ismember(events.trial_type, trial_types, 'rows');
        end
    end
end
num_runs=k;
starts=zeros(2,num_runs,5);
for j=1:5
    for i=1:k
        starts(:,i,j)=onset(trials(:,i)==j,i);
    end
end

conc_list={};
k=0;
for i=1:length(task_sessions)
    for j=1:2
        file_name = [subject '_' sessions(i,:) '_task-motor_run-' num2str(j) '_bold.dtseries.nii'];
        file_path = [BIDS_dir 'derivatives/surface/' subject '/task_cifti/' sessions(i,:) '/' file_name];
        if(exist(file_path,'file')==2)
            k=k+1;
            conc_list{k}=file_path;
        end
    end
end

conditions={'Tongue','LHand','RHand','LFoot','RFoot'};
window_width=int32(32*1.1/TR);
img=ft_read_cifti_mod(conc_list{1});
img_size=size(img.data,1);
disp('extracting time courses...')
for k=1:5
    disp(conditions{k});
    timecourses=zeros(img_size,window_width,num_runs*2);
    for i=1:num_runs
        img=ft_read_cifti_mod(conc_list{i});
        for j=1:2
            timecourses(:,:,(i*2+j-2))=img.data(:,(starts(j,i,k)):(starts(j,i,k)+window_width-1))-repmat(mean(img.data,2),1,window_width);
        end
    end

    save([outdir 'timecourses/' subject '_' conditions{k} '_timecourses.mat'],'timecourses','-v7.3');

    name=[subject '_' conditions{k} '_mean_timecourse'];
    mean_time=img;
    mean_time.data=mean(timecourses,3);
    ft_write_cifti_mod([outdir 'timecourses/' name '.dtseries.nii'],mean_time);
end
