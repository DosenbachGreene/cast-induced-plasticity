subjects={'MSC02','SIC02','SIC03'};

rep=2; % 0 = Face, 1 = Upper extremity, 2 = Lower extremity
roi_sets={[1 2]+14*rep, [7 8]+14*rep, [3 4]+14*rep}; % SM1, Cblm, SMA

p=NaN(1,3);
for set=1:length(roi_sets)
    roi1=roi_sets{set}(1);
    roi2=roi_sets{set}(2);

    % gather data
    cast=[];
    fc=[];
    subj=[];
    for s=1:3
        subject=subjects{s};
        switch subject
            case 'MSC02'
                pre_scannums=2:11;
                cast_scannums=12:24;
            case 'SIC02'
                pre_scannums=1:12;
                cast_scannums=13:26;
            case 'SIC03'
                pre_scannums=1:14;
                cast_scannums=15:28;
        end

        load(['/Users/newboldd/data/Cast_Paper/rois/' subject '_ROI_corrmat.mat']);
        r=squeeze(roi_corrmat(roi1,roi2,:));
        fc=[fc; (r([pre_scannums cast_scannums]))];
        cast_var=zeros(size([pre_scannums cast_scannums]))';
        cast_var(length(pre_scannums)+1:end)=1;
        cast=[cast; cast_var];
        subj=[subj; ones(length([pre_scannums cast_scannums]),1)*s];
    end

    % assemble table
    tbl=table(fc, cast, subj, 'VariableNames',{'FC','Cast','Subject'});
    tbl.Subject=nominal(tbl.Subject);

    % fit linear mixed-effects model
    lme=fitlme(tbl,'FC ~ Cast + (Cast|Subject)');
    p(set)=coefTest(lme);
end
% print p-values
p
% Benj-Hoch correction
sig=FDR(p)