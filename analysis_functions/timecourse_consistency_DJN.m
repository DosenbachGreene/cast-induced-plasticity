function timecourse_consistency_DJN(subject,outdir)

% uses data from task_timecourses_DJN.m 
% computes pairwise correlations betweeen all runs of a single voxel
% then averages the correlations together to compute time course
% consistency at each vertex
% the output is a surface file

disp(['intertrial_sync_SIC: ' subject])
if (exist([outdir 'sync'],'dir')~=7)
    system(['mkdir ' outdir 'sync']);
end
    
conditions={'Tongue','LHand','RHand','LFoot','RFoot'};
for j=1:5
    disp(conditions{j})
    load([outdir 'timecourses/' subject '_' conditions{j} '_timecourses.mat']);
    tc=zeros(size(timecourses,1),1);
    tri=logical(triu(ones(size(timecourses,3)),1));
    for i=1:size(timecourses,1)
        r=paircorr_mod(permute(timecourses(i,:,:),[2 3 1]));
        tc(i)=nanmean(r(tri));
    end
    name=[subject '_' conditions{j} '_sync'];
    temp=ft_read_cifti_mod([outdir 'timecourses/' subject '_Tongue_mean_timecourse.dtseries.nii']);
    temp.data=tc;
    ft_write_cifti_mod([outdir 'sync/' name '.dtseries.nii'],temp);
end

end