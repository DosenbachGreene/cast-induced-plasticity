# Cast-induced Plasticity

The scripts included here analyze data, recreate figures, and perform statistical analyses from the cast-induced plasticity study, described in Newbold et al., Plasticity and Spontaneous Activity Pulses in Disused Human Brain Circuits, Neuron (2020) https://doi.org/10.1016/j.neuron.2020.05.007

## Settings

- cast_settings.m

This file contains paths and processing parameters used by cast_analyses.m,
cast_figures.m and cast_stats.m. Edit this file to set paths or change settings

## Preprocessing

- preproc_cast1.csh
- preproc_cast2.csh
- preproc_cast3.csh

These scripts take in raw data, available at https://OpenNeuro.org/datasets/ds002766.
Each of the three subjects has their own preprocessing script and paramater file.
These scripts depend on tools included in the following packages:
- Freesurfer (v5.3): https://surfer.nmr.mgh.harvard.edu/fswiki/DownloadAndInstall
- Human Connectome Project (HCP) Workbench (v1.2.3): https://www.humanconnectome.org/software/get-connectome-workbench
- FSL Software Library (v5.0.6): https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation
- 4dfp tools: https://github.com/DosenbachGreene/4dfp_tools
- Custom dependencies are contained in the preproc_functions/ directory

Currently, all preproc scripts assume data is organized as output by a Siemens
Prisma MRI scanner. After initial processing, data were reorganized into BIDS
format for upload to OpenNeuro (https://OpenNeuro.org/datasets/ds002766). We
hope to offer BIDS-compatible preprocessing scripts in the near future. Fully
preprocessed data are available, in volume and surface space, in the derivatives/
directory on OpenNeuro.

## Secondary analyses

- cast_analyses.m

This script generates ROIs based on task and resting-state fMRI data, computes
functional connectivity (FC) matrices and seed maps, and creates all other files
required for figures and statistical analyses. Requires processed cifti-space
data, available in the derivatives/ folder on OpenNeuro
(https://OpenNeuro.org/datasets/ds002766). These files can also be generated
from raw data using the preproc_cast*.csh scripts described above.
Dependencies are contained in the analysis_functions/ directory.

## Statistical analyses

- cast_stats.m

This script creates a new directory called "statistics" and writes out text
files for all statistical analyses reported in Newbold et. al. 2020.

## Figures

- cast_figures.m

Recreates images from Newbold et. al. 2020. Dependencies are contained in
figure_functions.
