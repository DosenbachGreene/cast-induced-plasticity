function cast_bargraph_DJN(data,cast_color,bars_to_color,range,show_errorbars,show_dots)

spread = 0.1;

figure('position',[300 300 500 400])
hold on

X=1:length(data);
Y=nan(size(X));
for i=1:length(data)
    Y(i)=nanmean(data{i});
end
bar(X,Y,'FaceColor',[.98 .98 .98])
bar(X(bars_to_color),Y(bars_to_color),'FaceColor',cast_color)

if (show_errorbars)
    sem = @(x) std(x(~isnan(x)))/sqrt(sum(~isnan(x)));
    E=[sem(data{1}) sem(data{2}) sem(data{3})];
    ha = errorbar(X,Y,E,'LineStyle','none','LineWidth',2,'Color','k');
end

if (show_dots)
    x=[X(1)*ones(size(data{1})) X(2)*ones(size(data{2})) X(3)*ones(size(data{3}))];
    x=x+randn(size(x))*spread;
    scatter(x,[data{1} data{2} data{3}],'k');
end

axis([0 length(X)+1 range])