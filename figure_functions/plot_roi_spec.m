function [roi_spec, roi_alff, change, p] = plot_roi_spec (roi_time,roi_num,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR)

smoothness=30;
alff_freq_range=[0.05 0.1];

roi_spec = zeros(size(roi_time,1), rest_frames/2+1, size(roi_time,3));
for i=1:size(roi_time,3)
   P2=abs(fft(roi_time(:,:,i)')');
   P1=P2(:,1:rest_frames/2+1).^2;
   P1(:,2:end-1)=2*P1(:,2:end-1)/rest_frames;
   roi_spec(:,:,i)=P1;
end

% plot
spec=permute(roi_spec(roi_num,:,:),[2 3 1]);
smooth_spec=zeros(size(spec)-[2 0]);
for i=1:size(spec,2)
   smooth_spec(:,i)=smooth(spec(2:end-1,i),smoothness);
end
figure;
imagesc(flip(smooth_spec(1:400,[pre_scannums cast_scannums post_scannums]),1).^.5,[0 10])
colormap(jet)
f = (0:rest_frames/2-1)/rest_frames/TR;
plotted_f=f(2:401);
yticklabels=0.05:0.05:0.2;
yticks=zeros(size(yticklabels));
for i=1:length(yticklabels)
   [~,yticks(i)]=min(abs(yticklabels(i)-plotted_f));
end
set(gca,'ytick',yticks)
set(gca,'yticklabel',flip(yticklabels))
ylabel('Frequency (Hz)')
xlabel('Day')



f_in_range=f>alff_freq_range(1) & f<alff_freq_range(2);
roi_alff=sqrt(mean(roi_spec(:,f_in_range,:),2));
roi_alff=permute(roi_alff,[1 3 2]);
all_scans=[pre_scannums cast_scannums post_scannums];
figure('position',[100 100 500 200])
hold on
area([min(cast_scannums) max(cast_scannums)]+[-.5 .5],[1 1]*9.8,0.2,'facecolor',cast_color,'edgecolor','none','showbaseline','off')
scatter(all_scans,roi_alff(roi_num,all_scans),'k')
axis([0 65 0 10])
[~,p]=ttest2(roi_alff(roi_num,cast_scannums), roi_alff(roi_num,pre_scannums));
change=(mean(roi_alff(roi_num,cast_scannums))/mean(roi_alff(roi_num,pre_scannums))-1)*100;
title(['+' num2str(change) '% ; p = ' num2str(p)])