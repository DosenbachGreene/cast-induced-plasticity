function r = plasticity_model_2alpha(data, duration, cast_onset, cast_offset, params)

r0=mean(data(1:floor(cast_onset/24)));
rc=params(1);
alpha1=params(2);
alpha2=params(3);
alpha=zeros(1,duration);
alpha(cast_onset:cast_offset-1)=alpha1;
alpha(cast_offset:end)=alpha2;

% initialize FC
r=zeros(1,duration);
r(1)=r0;

B=zeros(1,duration);
B(1:cast_onset-1)=r0;
B(cast_onset:cast_offset)=rc;
B(cast_offset+1:end)=r0;

% run model
t=1;
while t<duration
    dr(t)=alpha(t)*(B(t)-r(t));
    r(t+1)=r(t)+dr(t);
    t=t+1;
end
