function [motor_corrmat,communities]=SIC_spring_embedding(subject, bins, parcel_corrmat, communities, parcel_path, motor_parcel_path,file_suffix,outdir,cast_color,sort_corrmat)

bin_names = {'pre','cast','post'};

edge_thresh=0.3;
L0=10;
Kc=10;

parcels=ft_read_cifti_mod(parcel_path);
parcel_list=unique(parcels.data(parcels.data>0));
struct=parcels.brainstructure(parcels.brainstructure>0);
assert(length(parcels.data)==length(struct));
hems=zeros(size(parcel_list));
for i=1:length(parcel_list)
    hems(i)=mode(struct(parcels.data==parcel_list(i)));
end
if (sort_corrmat)
    [~,sorti]=sort(hems);
    parcel_list=parcel_list(sorti);
end

right_hand_color=[0 1 1];
left_hand_color=[0 127 178]/255;
tongue_color=[1 0.5 0];
foot_color=[.13 .55 .13];
CON_color=[.5 0 .5];

motor_parcels=ft_read_cifti_mod(motor_parcel_path);
right_hand_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==1))));
left_hand_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==2))));
right_tongue_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==5))));
left_tongue_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==6))));
right_foot_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==3))));
left_foot_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==4))));
CON_nodes=find(communities==9);
motor_nodes=[right_hand_nodes; left_hand_nodes; right_tongue_nodes; left_tongue_nodes; right_foot_nodes; left_foot_nodes; CON_nodes];


for bin=1:length(bins)
    sessions=bins{bin};
    motor_corrmat=parcel_corrmat(motor_nodes,motor_nodes,sessions);
    communities=ones(length(motor_nodes),1); communities(1:length(right_hand_nodes))=2;
    mod(bin)=(modularity(mean(motor_corrmat,3),communities));
    graph=mean(motor_corrmat,3)>edge_thresh;

    right_hand_colors=repmat(right_hand_color,length(right_hand_nodes),1);
    left_hand_colors=repmat(left_hand_color,length(left_hand_nodes),1);
    tongue_colors=repmat(tongue_color,length(right_tongue_nodes)+length(left_tongue_nodes),1);
    foot_colors=repmat(foot_color,length(right_foot_nodes)+length(left_foot_nodes),1);
    CON_colors=repmat(CON_color,length(CON_nodes),1);
%    colors=[right_hand_colors; left_hand_colors; tongue_colors; foot_colors];
    colors=[right_hand_colors; left_hand_colors; tongue_colors; foot_colors; CON_colors];
%    colors=[right_hand_colors; CON_colors];

    nodes = size(graph,1);

    %Define circle
    r = L0;
    theta = 0:2*pi/nodes:2*pi-2*pi/nodes;
    nodepos = [r*sin(theta); r*cos(theta)]';

    %Calculate degree and those nodes with no edges
    for n = 1:nodes
        row = zeros(1,nodes);
        row(n) = 1;
        degree(n) = sum(graph(n,~row));
    end
    outnodeind = find(degree==0);

    % Find shortest path using dijkstra algorithm
    D = dijkstra(graph,graph);
    D(isinf(D))=0;
    %D = graph;

    % Find desirable length between nodes
    maxd = max(D(:));
    L = L0/maxd;
    I = L.*D;

    % Strength of springs between nodes
    K = Kc./(D.^2);
    K(isinf(K))=0;
    %
    % for m = 1:nodes
    %     xm = repmat(nodepos(m,1),nodes-1,1);
    %     ym = repmat(nodepos(m,2),nodes-1,1);
    %     xother = nodepos(:,1);
    %     xother(m) = [];
    %     yother = nodepos(:,2);
    %     yother(m) = [];
    %     Im = I(m,:)';
    %     Im(m) = [];
    %     Km = K(m,:)';
    %     Km(m) = [];
    %
    %     dEdxm = sum(Km.*(xm-xother)-((Im.*(xm-xother))./((xm-xother).^2 + (ym - yother).^2).^.5));
    %     dEdym = sum(Km.*(ym-yother)-((Im.*(ym-yother))./((xm-xother).^2 + (ym - yother).^2).^.5));
    %     deltam(m) = sqrt((dEdxm).^2+(dEdym).^2);
    % end


    xm = repmat(nodepos(:,1),1,nodes);
    xm(logical(eye(nodes))) = 0;
    ym = repmat(nodepos(:,2),1,nodes);
    ym(logical(eye(nodes))) = 0;
    xother = repmat(nodepos(:,1),1,nodes)';
    xother(logical(eye(nodes))) = 0;
    yother = repmat(nodepos(:,2),1,nodes)';
    yother(logical(eye(nodes))) = 0;

    Im = I;
    Km = K;

    dEdxmstep = Km.*((xm-xother)-((Im.*(xm-xother))./((xm-xother).^2 + (ym - yother).^2).^.5));
    dEdxmstep(isnan(dEdxmstep))=0;
    dEdxm = sum(dEdxmstep,2);
    dEdymstep = Km.*((ym-yother)-((Im.*(ym-yother))./((xm-xother).^2 + (ym - yother).^2).^.5));
    dEdymstep(isnan(dEdymstep))=0;
    dEdym = sum(dEdymstep,2);
    deltam = sqrt((dEdxm).^2+(dEdym).^2)';

    count = 1;
    while max(deltam)>.05
        count
        if count>10000
            break
        end
        [maxval maxi] = max(deltam);
        xm = repmat(nodepos(maxi,1),nodes-1,1);
        ym = repmat(nodepos(maxi,2),nodes-1,1);
        xother = nodepos(:,1);
        xother(maxi) = [];
        yother = nodepos(:,2);
        yother(maxi) = [];
        Im = I(maxi,:)';
        Im(maxi) = [];
        Km = K(maxi,:)';
        Km(maxi) = [];
        dEdxm = sum(Km.*((xm-xother)-((Im.*(xm-xother))./((xm-xother).^2 + (ym - yother).^2).^.5)));
        dEdym = sum(Km.*((ym-yother)-((Im.*(ym-yother))./((xm-xother).^2 + (ym - yother).^2).^.5)));

        d2Edxm2 = sum(Km.*(1-((Im.*(ym-yother).^2)./((xm-xother).^2+(ym-yother).^2).^(3/2))));
        d2Edym2 = sum(Km.*(1-((Im.*(xm-xother).^2)./((xm-xother).^2+(ym-yother).^2).^(3/2))));

        d2Edxmdym = sum(Km.*((Im.*(xm-xother).*(ym-yother))./((xm-xother).^2+(ym-yother).^2).^(3/2)));
        d2Edymdxm = sum(Km.*((Im.*(xm-xother).*(ym-yother))./((xm-xother).^2+(ym-yother).^2).^(3/2)));

        A = [d2Edxm2 d2Edxmdym; d2Edymdxm d2Edym2];
        B = [-dEdxm;-dEdym];

        X = linsolve(A,B);
        deltax = X(1);
        deltay = X(2);
        nodepos(maxi,1) = nodepos(maxi,1)+deltax;
        nodepos(maxi,2) = nodepos(maxi,2)+deltay;

        %     for m = 1:nodes
        %         xm = repmat(nodepos(m,1),nodes-1,1);
        %         ym = repmat(nodepos(m,2),nodes-1,1);
        %         xother = nodepos(:,1);
        %         xother(m) = [];
        %         yother = nodepos(:,2);
        %         yother(m) = [];
        %         Im = I(m,:)';
        %         Im(m) = [];
        %         Km = K(m,:)';
        %         Km(m) = [];
        %
        %         dEdxm = sum(Km.*((xm-xother)-((Im.*(xm-xother))./((xm-xother).^2 + (ym - yother).^2).^.5)));
        %         dEdym = sum(Km.*((ym-yother)-((Im.*(ym-yother))./((xm-xother).^2 + (ym - yother).^2).^.5)));
        %         deltam(m) = sqrt((dEdxm).^2+(dEdym).^2);
        %     end

        xm = repmat(nodepos(:,1),1,nodes);
        xm(logical(eye(nodes))) = 0;
        ym = repmat(nodepos(:,2),1,nodes);
        ym(logical(eye(nodes))) = 0;
        xother = repmat(nodepos(:,1),1,nodes)';
        xother(logical(eye(nodes))) = 0;
        yother = repmat(nodepos(:,2),1,nodes)';
        yother(logical(eye(nodes))) = 0;

        Im = I;
        Km = K;

        dEdxmstep = Km.*((xm-xother)-((Im.*(xm-xother))./((xm-xother).^2 + (ym - yother).^2).^.5));
        dEdxmstep(isnan(dEdxmstep))=0;
        dEdxm = sum(dEdxmstep,2);
        dEdymstep = Km.*((ym-yother)-((Im.*(ym-yother))./((xm-xother).^2 + (ym - yother).^2).^.5));
        dEdymstep(isnan(dEdymstep))=0;
        dEdym = sum(dEdymstep,2);
        deltam = sqrt((dEdxm).^2+(dEdym).^2)';

        %     h=figure('position',[1500 500 700 500]);
        %     plot(nodepos(:,1),nodepos(:,2),'r.','MarkerSize',30)
        %
        %     plot(nodepos(:,1),nodepos(:,2),'b.','MarkerSize',30)
        %
        %     for i = 1:size(graph,1)
        %         initcoordmat = repmat(nodepos(i,:),nnz(graph(i,:)),1);
        %         line([initcoordmat(:,1) nodepos(logical(graph(i,:)),1)]',[initcoordmat(:,2) nodepos(logical(graph(i,:)),2)]','Color','r');
        %     end
        count = count+1;
    end

    h=figure('position',[1500 500 750 555]);
    hold
    axis([-r r -r r])

    %Paste nodes not in network at bottom
    outnodepos = -r:(2*r)/(length(outnodeind)-1):r;
    for j = 1:length(outnodeind)
        nodepos(outnodeind(j),:) = [outnodepos(j) -r];
    end

    %Draw lines between nodes
    for i = 1:size(graph,1)
        initcoordmat = repmat(nodepos(i,:),nnz(graph(i,:)),1);
        line([initcoordmat(:,1) nodepos(logical(graph(i,:)),1)]',[initcoordmat(:,2) nodepos(logical(graph(i,:)),2)]','Color',[105/255 105/255 105/255]);
    end

    %Mark and color node positions
    int=1;
    for i = 1:nodes
        plot(nodepos(int,1),nodepos(int,2),'MarkerFaceColor',[colors(i,1) colors(i,2) colors(i,3)],'Marker','o','MarkerSize',8,'MarkerEdgeColor','k','LineWidth',1);
        int = int+1;
    end
    int=1;
    for i = 1:length(right_hand_nodes)
        plot(nodepos(int,1),nodepos(int,2),'MarkerFaceColor',[colors(i,1) colors(i,2) colors(i,3)],'Marker','o','MarkerSize',12,'MarkerEdgeColor','k','LineWidth',4);
        int = int+1;
    end
    int=length(right_hand_nodes)+length(left_hand_nodes)+1;
    for i = int:(int+length(right_tongue_nodes)-1)
        plot(nodepos(int,1),nodepos(int,2),'MarkerFaceColor',[colors(i,1) colors(i,2) colors(i,3)],'Marker','o','MarkerSize',8,'MarkerEdgeColor','k','LineWidth',3);
        int = int+1;
    end
    int=length(right_hand_nodes)+length(left_hand_nodes)+length(right_tongue_nodes)+length(left_tongue_nodes)+1;
    for i = int:(int+length(right_foot_nodes)-1);
        plot(nodepos(int,1),nodepos(int,2),'MarkerFaceColor',[colors(i,1) colors(i,2) colors(i,3)],'Marker','o','MarkerSize',8,'MarkerEdgeColor','k','LineWidth',3);
        int = int+1;
    end
    %
    %     % For module coloring
    %      int=1;
    %      for m = 1:length(mods)
    %          for i = 1:length(mods{m})
    %              plot(nodepos(int,1),nodepos(int,2),'MarkerFaceColor',[colors(m,1) colors(m,2) colors(m,3)],'Marker','o','MarkerSize',8,'MarkerEdgeColor','k','LineWidth',2);
    %              int = int+1;
    %          end
    %      end
    %     for i = 1:length(nodepos)
    %         text(nodepos(i,1),nodepos(i,2),[num2str(seeds(i,1)) '/' num2str(seeds(i,2)) '/' num2str(seeds(i,3))]);
    %     end
    %saveas(h,['/data/cn4/laumannt/dyn_spring_fig_water/fig' num2str(t) ],'tiff')
    %M(t) = getframe(h);
    %close(h)
    set(gca,'color','none')
    export_fig([outdir subject '_' bin_names{bin} file_suffix '.eps'],'-transparent')
    close(h)
end
