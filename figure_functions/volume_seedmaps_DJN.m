function volume_seedmaps_DJN (basedir, roi_time, rest_sesslist, tmask)

sessions=tdfread(rest_sesslist);
sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'pre','cast','post'}));
for i=1:length(sessions)
    ses=sessions{i};
    disp([num2str(i) ': ' ses])
    time=read_4dfpimg([basedir ses '/bold1/' ses '_b1_faln_dbnd_xr3d_uwrp_atl_bpss_resid_smooth2.4dfp.img']);
    seeds(:,i)=corr(time(:,tmask(i,:))',roi_time(15,tmask(i,:),i)');
end

write_4dfpimg(seeds,[outpath '.4dfp.img'],'bigendian');
write_4dfpifh([outpath '.4dfp.ifh'],i,'bigendian');
system(['niftigz_4dfp -n ' outpath ' ' outpath]);

end