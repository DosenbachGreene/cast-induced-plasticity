function motor_roi_labels_DJN(subj, roi_dir)
% makes dlabel files to display motor ROIs

% Hand
left_M1_color=[20 200 200]/255;
right_M1_color=[10 80 120]/255;
M1_colors=[left_M1_color; right_M1_color; left_M1_color; right_M1_color; left_M1_color; right_M1_color; right_M1_color; left_M1_color; left_M1_color; right_M1_color];

rois=ft_read_cifti_mod([roi_dir subj '_Hand_ROIs.dtseries.nii']);
new_rois=zeros(size(rois.data));
new_rois(rois.data==1)=1;
new_rois(rois.data==2)=2;
new_rois(rois.data==3)=3;
new_rois(rois.data==4)=4;
new_rois(rois.data==5)=5;
new_rois(rois.data==6)=6;
new_rois(rois.data==7)=7;
new_rois(rois.data==8)=8;
new_rois(rois.data==13)=9;
new_rois(rois.data==14)=10;
rois.data=new_rois;
ft_write_cifti_mod([roi_dir subj '_Hand_M1'],rois)
make_cifti_label([roi_dir subj '_Hand_M1.dtseries.nii'],M1_colors);
    
% Foot
left_M1_color=[20 140 20]/255;
right_M1_color=[10 90 10]/255;
M1_colors=[left_M1_color; right_M1_color; right_M1_color; left_M1_color; left_M1_color; right_M1_color];

rois=ft_read_cifti_mod([roi_dir subj '_Foot_ROIs.dtseries.nii']);
new_rois=zeros(size(rois.data));
new_rois(rois.data==1)=1;
new_rois(rois.data==2)=2;
new_rois(rois.data==7)=3;
new_rois(rois.data==8)=4;
new_rois(rois.data==13)=5;
new_rois(rois.data==14)=6;
rois.data=new_rois;
ft_write_cifti_mod([roi_dir subj '_Foot_M1'],rois)
make_cifti_label([roi_dir subj '_Foot_M1.dtseries.nii'],M1_colors);

% Tongue
left_M1_color=[255 130 0]/255;
right_M1_color=[130 65 0]/255;
M1_colors=[left_M1_color; right_M1_color; right_M1_color; left_M1_color; left_M1_color; right_M1_color];

rois=ft_read_cifti_mod([roi_dir subj '_Tongue_ROIs.dtseries.nii']);
new_rois=zeros(size(rois.data));
new_rois(rois.data==1)=1;
new_rois(rois.data==2)=2;
new_rois(rois.data==7)=3;
new_rois(rois.data==8)=4;
new_rois(rois.data==13)=5;
new_rois(rois.data==14)=6;
rois.data=new_rois;

ft_write_cifti_mod([roi_dir subj '_Tongue_M1'],rois)
make_cifti_label([roi_dir subj '_Tongue_M1.dtseries.nii'],M1_colors);



