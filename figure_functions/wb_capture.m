function wb_capture(subj, cifti, scale_bar, thresh, frame_nums, frame_names, shape)

%subj='SIC02';
%cifti='/data/nil-bluearc/GMT/Dillan/preproc_2018-07-03/SIC02/cifti_timeseries_normalwall/vc41379_b1_faln_dbnd_xr3d_uwrp_atl_bpss_resid_LR_surf_subcort_333_32k_fsLR_surfsmooth2.55_volsmooth2.dtseries.nii';
%cifti='/data/nil-bluearc/GMT/Dillan/Whole_Brain_Paper/seedmaps/ttests/SIC02_L-SM1ue_tstat.dtseries.nii';

if ~exist('frame_nums','var') || isempty(frame_nums)
    frame_nums=1;
end
if ~exist('thresh','var') || isempty(thresh)
    thresh=[0 0];
elseif length(thresh)==1
    thresh=[-abs(thresh) abs(thresh)];
end
if length(scale_bar)==1
    scale_bar=[-abs(scale_bar) abs(scale_bar)];
end
if ~exist('shape','var') || isempty(shape)
    shape='square';
end
wb_command = '/Users/newboldd/Projects/bin_macosx64//wb_command';
imagedir='/Users/newboldd/data/Cast_Paper/wb_figures/';
%boxsync_dir = '/data/nil-bluearc/GMT/Dillan/figures/';
scene_template=[imagedir 'template.scene'];
[~,cifti_name,~]=fileparts(cifti);
[~,cifti_name,cifti_type]=fileparts(cifti_name);
if isempty(cifti_type)
    cifti_type = '.dtseries';
end
assert(~isempty(cifti_name))

% create output directory
outdir=[imagedir cifti_name '/'];
if (exist(outdir,'dir')==7)
    system(['rm -rf ' outdir]);
end
system(['mkdir ' outdir]);
scene_file=[outdir cifti_name '.scene'];

% adjust cifti path
cifti_path=fileparts(cifti);
if isempty(cifti_path)
    cifti=[pwd '/' cifti_name];
elseif cifti_path(1) ~= '/'
    cifti=[pwd '/' cifti_path '/' cifti_name];
else
    cifti=[cifti_path '/' cifti_name];
end
dynconn = [cifti '.dynconn.nii'];
cifti = [cifti cifti_type '.nii'];
assert(exist(cifti,'file')==2, 'Error: could not find cifti')

% set paths
base_dir=['/Users/newboldd/data/cast_preproc/' subj '/'];
lsurf=[base_dir 'anatomy/' subj '.L.inflated.32k_fs_LR.surf.gii'];
rsurf=[base_dir 'anatomy/' subj '.R.inflated.32k_fs_LR.surf.gii'];
T1=[base_dir 'anatomy/' subj '_mpr_debias_avgT_111_t88.nii.gz'];        

% adjust paths for sed
lsurf=regexprep(lsurf,'/','\\/');
rsurf=regexprep(rsurf,'/','\\/');
T1=regexprep(T1,'/','\\/');
dynconn=regexprep(dynconn,'/','\\/');
cifti=regexprep(cifti,'/','\\/');

%re-write scene file
for i=1:length(frame_nums)
    frame_num=frame_nums(i);
    system(['cat ' scene_template ' | sed ''s/{subject}/' subj '/g'' > ' scene_file]);
    system(['sed -i '''' ''s/{T1_path}/' T1 '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{rsurf_path}/' rsurf '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{lsurf_path}/' lsurf '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{cifti_path}/' cifti '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{dynconn_path}/' dynconn '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{scale_bar_upper}/' num2str(scale_bar(2)) '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{scale_bar_lower}/' num2str(scale_bar(1)) '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{thresh_upper}/' num2str(thresh(2)) '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{thresh_lower}/' num2str(thresh(1)) '/g'' ' scene_file]);
    system(['sed -i '''' ''s/{frame_num}/' num2str(frame_num-1) '/g'' ' scene_file]);
    
    %capture images
    cort_png=[outdir 'cortex.png'];
    bgt_png=[outdir 'BGT.png'];
    lcblm_png=[outdir 'R-Cblm.png'];
    rcblm_png=[outdir 'L-Cblm.png'];
    system([wb_command ' -show-scene ' scene_file ' surf_inflated ' cort_png ' 900 600']);
    system([wb_command ' -show-scene ' scene_file ' BGT_axial ' bgt_png ' 600 600']);
    system([wb_command ' -show-scene ' scene_file ' R-Cblm ' rcblm_png ' 600 600']);
    system([wb_command ' -show-scene ' scene_file ' L-Cblm ' lcblm_png ' 600 600']);

    % combine images
    
    if length(frame_nums) == 1
        out_png=[cifti_name '.png'];
    elseif ~exist('frame_names','var') || isempty(frame_names)
        out_png=[cifti_name '_frame' num2str(frame_nums) '.png'];
    else
        out_png=[cifti_name '_' frame_names{i} '.png'];
    end
    switch shape
        case 'square'
            subcort_png=[outdir 'subcortical.png'];
            system(['/usr/local/bin/montage -geometry 600x600+0+0 ' bgt_png ' ' lcblm_png ' ' rcblm_png ' ' subcort_png]); 
            system(['/usr/local/bin/convert ' subcort_png ' -resize 900x600 ' subcort_png]);
            system(['/usr/local/bin/montage -geometry +0+0 -tile 1x2 ' cort_png ' ' subcort_png ' ' outdir out_png]);
            system(['cp ' outdir out_png ' ' boxsync_dir out_png]);
        case 'stack'
            borderw=20;
            borderh=10;
            lh_png=[outdir 'lh.png'];
            rh_png=[outdir 'rh.png'];
            system(['/usr/local/bin/convert ' cort_png ' -crop 450x600+0+0 ' lh_png])
            system(['/usr/local/bin/convert ' cort_png ' -crop 450x600+450+0 ' rh_png])
            system(['/usr/local/bin/convert ' rh_png ' -resize 600x800 ' rh_png])
            system(['/usr/local/bin/convert ' lh_png ' -resize 600x800 ' lh_png])
            %system(['convert ' bgt_png ' -crop 300x300+150+150 ' bgt_png])
            system(['/usr/local/bin/convert ' bgt_png ' -bordercolor white -border ' num2str(borderw) 'x' num2str(borderh) ' ' bgt_png])
            system(['/usr/local/bin/convert ' bgt_png ' -resize 600x' num2str(round(600*(600+2*borderh)/(600+2*borderw))) ' ' bgt_png])
            system(['/usr/local/bin/convert ' rcblm_png ' -crop 600x400+0+200 ' rcblm_png])
            system(['/usr/local/bin/convert ' lcblm_png ' -crop 600x400+0+200 ' lcblm_png])
            system(['/usr/local/bin/convert ' rcblm_png ' -bordercolor white -border ' num2str(borderw) 'x' num2str(borderh) ' ' rcblm_png])
            system(['/usr/local/bin/convert ' lcblm_png ' -bordercolor white -border ' num2str(borderw) 'x' num2str(borderh) ' ' lcblm_png])
            system(['/usr/local/bin/convert ' rcblm_png ' -resize 600x' num2str(round(600*(400+2*borderh)/(600+2*borderw))) ' ' rcblm_png])
            system(['/usr/local/bin/convert ' lcblm_png ' -resize 600x' num2str(round(600*(400+2*borderh)/(600+2*borderw))) ' ' lcblm_png])
            system(['/usr/local/bin/montage -geometry +0+0 -tile 1x5 ' lh_png ' ' rh_png ' ' bgt_png ' ' rcblm_png ' ' lcblm_png ' ' outdir out_png])
            %system(['cp ' outdir out_png ' ' boxsync_dir out_png]);
    end
    
    % show image
    [png, colormap]=imread([outdir out_png]);
    figure; imshow(png, colormap)
end
end