settings_dir='/Users/newboldd/data/Cast_Paper/';

subjects = {'sub-cast1', 'sub-cast2', 'sub-cast3'};
subjects_to_run = [1 2 3];

% Main figures
accel_figs                  = 1;
strength_figs               = 1;
volume_seedmap_figs         = 1;
roi_figs                    = 1;
fc_timecourse_figs          = 1;
spring_embed_figs           = 1;
modularity_figs             = 1;
alff_figs                   = 1;
roi_timecourse_figs         = 1;
pulse_timecourse_figs       = 1;
pulse_count_figs            = 1;
propagation_example_figs    = 1;
pulse_timing_figs           = 1;

% Supplemental figures
pegboard_figs               = 1;
cast_onoff_figs             = 1;
hand_movement_figs          = 1;

% loop through subjects
for s=subjects_to_run
   subject = subjects{s};
   disp(subject)
   
   % load settings
   load([settings_dir subject '_settings.mat'])

   % accelerometry
   if accel_figs
       load([accel_outdir subject '_use_counts.mat'],'lu','ru','accel_cast_period');
       pre=1:(accel_cast_period(1)-1);
       cast=accel_cast_period(1):accel_cast_period(2);
       post=(accel_cast_period(2)+1):length(lu);

       % use ratio
       figure; hold on;
       area(accel_cast_period + [-.5 .5],[1.48 1.48], .02, 'FaceColor', cast_color,'LineStyle','none','showbaseline','off');
       axis([0 70 0 1.5]);
       scatter(1:length(ru),ru./lu,50,'k');
       ylabel('Use ratio')
       xlabel('Day')

       % left use counts
       bars_to_color=2;
       cast_bargraph_DJN({lu(pre), lu(cast), lu(post)},bars_to_color,cast_color,[0 0.7],1,1);

       % right use counts
       cast_bargraph_DJN({ru(pre), ru(cast), ru(post)},cast_color,bars_to_color,[0 0.7],1,1);
   end

   % grip strength
   if (strength_figs)
        load([strength_dir subject '_grip_right.mat'],'grip','pre','post','dates');
        % remove any tiem points prior to pre(1)
        grip(1:(pre(1)-1),:)=[];
        dates(1:(pre(1)-1))=[];
        post=post-pre(1)+1;
        pre=pre-pre(1)+1;

        days=datenum(dates);
        days=days-days(1)+1;

        Y=nanmean(grip,2);
        E=[];
        for i=1:length(Y)
            if (~isnan(Y(i)))
                E(i)=std(grip(i,~isnan(grip(i,:))))/sqrt(sum(~isnan(grip(i,:))));
            else
                E(i)=NaN;
            end
        end
        figure;
        hold on
        area([days(pre(end))+0.5 days(post)-0.5], [138 138], 2, 'FaceColor', cast_color, 'LineStyle', 'none','showbaseline','off');
        ha = errorbar(days,Y,E,'LineWidth',2,'color','k');

        load([strength_dir subject '_grip_left.mat'],'grip','pre','post','dates');
        % remove any tiem points prior to pre(1)
        grip(1:(pre(1)-1),:)=[];
        dates(1:(pre(1)-1))=[];
        pre=pre-pre(1)+1;
        post=post-pre(1)+1;
        Y=nanmean(grip,2);
        E=[];
        for i=1:length(Y)
            if (~isnan(Y(i)))
                E(i)=std(grip(i,~isnan(grip(i,:))))/sqrt(sum(~isnan(grip(i,:))));
            else
                E(i)=NaN;
            end
        end

        ha = errorbar(days,Y,E,'LineWidth',2,'color',[.5 .5 .5],'linestyle','--');

        axis([0 70 0 140])
   end

   % volume seedmaps
   if volume_seedmap_figs
       load([roi_outdir subject '_ROI_timecourses.mat'],'roi_time')
       load([basedir 'tmask.mat'],'tmask')
       volume_seedmaps_DJN(basedir,roi_time,rest_sesslist,tmask,[seedmap_outdir subject '_volume_seedmaps_smooth2'])
       % outputs a NIFTI file. Figures were rendered using the Human Connectome Project (HCP) Workbench
   end

   % regions of interest
   if roi_figs
       motor_roi_labels_PP(subject,roi_outdir)
       % outputs dlabel files. Figures were rendered using the Human Connectome Project (HCP) Workbench
   end

    % FC time courses
    if (fc_timecourse_figs)
        % Upper extremity
        roi1=1+6; roi2=2+6;
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
        data=squeeze(roi_corrmat(roi1,roi2,:));
        duration=length(data)*24;

        % fit exponential decay model model
        ObjectiveFunction = @(x)fit_plasticity_model_2alpha(data,duration, cast_onset, cast_offset, measurement_times, x);
        initial_params=[0 0.005 0.005];
        params_estimate=lsqnonlin(ObjectiveFunction,initial_params);

        figure;
        hold on;
        range = [-.4 1];
        area([cast_onset cast_offset], (range(2)-0.02)*[1 1], range(1)+0.02, 'FaceColor', cast_color, 'LineStyle', 'none','showbaseline','off');
        axis([0 65*24 range]);
        xL = get(gca, 'XLim');
        plot(xL, [0 0], '--','color',[.7 .7 .7])
        scatter(measurement_times,data,'ko');
        scatter(measurement_times(27),data(27),'ko');
        r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
        plot(1:duration,r,'k','linewidth',1.5);
        set(gca,'xtick',10*24:10*24:65*24)
        set(gca,'xticklabel',[])
        set(gca,'ytick',-.4:.2:1)
        set(gca,'yticklabel',[])

        % lower extremity
        roi1=1+6; roi2=2+6;
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
        data=squeeze(roi_corrmat(roi1,roi2,:));
        duration=length(data)*24;

        % fit exponential decay model model
        ObjectiveFunction = @(x)fit_plasticity_model_2alpha(data,duration, cast_onset, cast_offset, measurement_times, x);
        initial_params=[0 0.005 0.005];
        params_estimate=lsqnonlin(ObjectiveFunction,initial_params);

        figure;
        hold on;
        range = [-.4 1];
        area([cast_onset cast_offset], (range(2)-0.02)*[1 1], range(1)+0.02, 'FaceColor', cast_color, 'LineStyle', 'none','showbaseline','off');
        axis([0 65*24 range]);
        xL = get(gca, 'XLim');
        plot(xL, [0 0], '--','color',[.7 .7 .7])
        scatter(measurement_times,data,'ko');
        r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
        plot(1:duration,r,'k','linewidth',1.5);
        set(gca,'xtick',10*24:10*24:65*24)
        set(gca,'xticklabel',[])
        set(gca,'ytick',-.4:.2:1)
        set(gca,'yticklabel',[])

        % face
        roi1=1; roi2=2;
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
        data=squeeze(roi_corrmat(roi1,roi2,:));
        duration=length(data)*24;

        % fit exponential decay model model
        ObjectiveFunction = @(x)fit_plasticity_model_2alpha(data,duration, cast_onset, cast_offset, measurement_times, x);
        initial_params=[0 0.005 0.005];
        params_estimate=lsqnonlin(ObjectiveFunction,initial_params);

        figure;
        hold on;
        range = [-.4 1];
        area([cast_onset cast_offset], (range(2)-0.02)*[1 1], range(1)+0.02, 'FaceColor', cast_color, 'LineStyle', 'none','showbaseline','off');
        axis([0 65*24 range]);
        xL = get(gca, 'XLim');
        plot(xL, [0 0], '--','color',[.7 .7 .7])
        scatter(measurement_times,data,'ko');
        r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
        plot(1:duration,r,'k','linewidth',1.5);
        set(gca,'xtick',10*24:10*24:65*24)
        set(gca,'xticklabel',[])
        set(gca,'ytick',-.4:.2:1)
        set(gca,'yticklabel',[])

        % cerebellum
        roi1=3+6; roi2=4+6;
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
        data=squeeze(roi_corrmat(roi1,roi2,:));
        duration=length(data)*24;

        % fit exponential decay model model
        ObjectiveFunction = @(x)fit_plasticity_model_2alpha(data,duration, cast_onset, cast_offset, measurement_times, x);
        initial_params=[0 0.005 0.005];
        params_estimate=lsqnonlin(ObjectiveFunction,initial_params);

        figure;
        hold on;
        range = [-.4 1];
        area([cast_onset cast_offset], (range(2)-0.02)*[1 1], range(1)+0.02, 'FaceColor', cast_color, 'LineStyle', 'none','showbaseline','off');
        axis([0 65*24 range]);
        xL = get(gca, 'XLim');
        plot(xL, [0 0], '--','color',[.7 .7 .7])
        scatter(measurement_times,data,'ko');
        r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
        plot(1:duration,r,'k','linewidth',1.5);
        set(gca,'xtick',10*24:10*24:65*24)
        set(gca,'xticklabel',[])
        set(gca,'ytick',-.4:.2:1)
        set(gca,'yticklabel',[])
    end

   % Spring embedded plots of somatomotor network
   if spring_embed_figs
       if (exist(spring_embed_outdir,'dir')~=7)
            mkdir(spring_embed_outdir);
       end
       disp('making spring embedding plots')
       load([parcel_outdir subject '_parcel_corrmat.mat'],'parcel_corrmat','communities');
       parcel_path=[BIDS_dir 'derivatives/surface/' subject '/surface_parcellation/' subject '_parcels.dtseries.nii'];
       SIC_spring_embedding(subject, {pre_scannums, cast_scannums, post_scannums}, parcel_corrmat,communities,parcel_path, [parcel_outdir subject '_primary_somatomotor_parcels.dtseries.nii'],'',spring_embed_outdir,cast_color,sort_parcels);
   end

    % modularity bargraph
    if modularity_figs
        load([parcel_outdir subject '_parcel_corrmat.mat'],'parcel_corrmat','communities');
        parcels=ft_read_cifti_mod(parcel_path);
        parcel_list=unique(parcels.data(parcels.data>0));
        motor_parcels=ft_read_cifti_mod([parcel_outdir subject '_primary_somatomotor_parcels.dtseries.nii']);
        right_hand_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==1))));
        other_somatomotor=find(ismember(parcel_list,unique(parcels.data(ismember(motor_parcels.data,2:6)))));
        motor_nodes=[right_hand_nodes other_somatomotor_nodes];
        pre_graph=mean(parcel_corrmat(motor_nodes,motor_nodes,pre_scannums),3);
        cast_graph=mean(parcel_corrmat(motor_nodes,motor_nodes,cast_scannums),3);
        post_graph=mean(parcel_corrmat(motor_nodes,motor_nodes,post_scannums),3);
        communities=ones(size(motor_nodes));
        communities(1:length(right_hand_nodes))=2;
        bars_to_color=2;
        cast_bargraph_DJN({data(pre_scannums), data(cast_scannums), data(post_scannums)},cast_color,bars_to_color[-0.5 0.5],0,0);
        title('SMN modularity')
    end
    
    % spectral images and ALFF-by-day plots
    if alff_figs
       load ([roi_outdir subject '_ROI_timecourses.mat'],'roi_time')
       % L-SM1ue
       plot_roi_spec(roi_time,1+6,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
       % R-SM1ue
       plot_roi_spec(roi_time,2+6,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
       % L-SM1le
       plot_roi_spec(roi_time,1,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
       % L-SM1face
       plot_roi_spec(roi_time,1+12,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
    end
    
    % plot ROI timecourses for all scans
    if roi_timecourse_figs
        roi1=1; % L-SM1 (1 = L-SM1, 2 = R-SM1, 3/4 = L/R-SMA, 5/6 = L/R-Cblm)
        roi1=roi1+6; % Hand (0 = Tongue, 6 = Hand, 12 = Foot)
        roi2 = 2 + 6;
        load ([roi_outdir subject '_ROI_timecourses.mat'],'roi_time')
        plot_roi_time(roi1,roi2,roi_time,TR)
    end
    
    % plot pulses all pulses together
    if pulse_timecourse_figs
       load([tmask_outdir subject '_tmask.mat'],'tmask')
       load([detection_outdir subj '_pulse_list.mat'],'pulse_list','pulse_count')
       load([roi_outdir subj '_ROI_timecourses.mat'],'roi_time');

       roi1=1; % L-SM1 (1 = L-SM1, 2 = R-SM1, 3/4 = L/R-SMA, 5/6 = L/R-Cblm)
       roi1=roi1+6; % Hand (0 = Tongue, 6 = Hand, 12 = Foot)
       rois=[3 1 6]+6;
       roi_names={'SMA','SM1','Cblm'};
       reference_roi=2; % which ROI in "rois" should be used as a zero point for pulse timing
       plot_timecourses=true;
       plot_time_delays=false;

       pulse_propagation(tmask, pulse_list, pulse_count, roi_time, TR, subject, plot_timecourses, rois, roi_names, reference_roi, plot_time_delays)
    end
    
    % plot # pulses detected per scan
    if pulse_count_figs
       file_suffix='';
       load([tmask_outdir subject '_tmask' file_suffix '.mat'],'tmask')
       load([roi_outdir subject '_ROI_timecourses' file_suffix '.mat'],'roi_time')

       plot_pulse_locations=false;
       plot_pulse_counts=true;
       
       pulse_detection(tmask_spread, tmask, roi_time, roi1, roi2, thresh1, thresh2, plot_pulse_locations, plot_pulse_counts, file_suffix)
    end
    
    % plot individual pulses in SMA, SM1 + Cblm as example of propagation
    if propagation_example_figs
        
    end
    
    % plot timing of all pulses in SMA, SM1 + Cblm
    if pulse_timing_figs
       load([tmask_outdir subject '_tmask.mat'],'tmask')
       load([detection_outdir subj '_pulse_list.mat'],'pulse_list','pulse_count')
       load([roi_outdir subj '_ROI_timecourses.mat'],'roi_time');

       roi1=1; % L-SM1 (1 = L-SM1, 2 = R-SM1, 3/4 = L/R-SMA, 5/6 = L/R-Cblm)
       roi1=roi1+6; % Hand (0 = Tongue, 6 = Hand, 12 = Foot)
       rois=[3 1 6]+6;
       roi_names={'SMA','SM1','Cblm'};
       reference_roi=2; % which ROI in "rois" should be used as a zero point for pulse timing
       plot_timecourses=false;
       plot_time_delays=true;
       
       pulse_propagation(tmask, pulse_list, pulse_count, roi_time, TR, subject, plot_timecourses, rois, roi_names, reference_roi, plot_time_delays)
    end
    
    % plot results from Purdue Pegboard task
   if (pegboard_figs)
        load([pegboard_dir subject '_pegs_right.mat'],'pegs','pre','post','dates');
        % remove any tiem points prior to pre(1)
        pegs(1:(pre(1)-1),:)=[];
        dates(1:(pre(1)-1))=[];
        post=post-pre(1)+1;
        pre=pre-pre(1)+1;

        days=datenum(dates);
        days=days-days(1)+1;

        Y=nanmean(pegs,2);
        E=[];
        for i=1:length(Y)
            if (~isnan(Y(i)))
                E(i)=std(pegs(i,~isnan(pegs(i,:))))/sqrt(sum(~isnan(pegs(i,:))));
            else
                E(i)=NaN;
            end
        end
        skip=isnan(Y);
        Y(skip)=[];
        days(skip)=[];
        E(skip)=[];
        figure;
        hold on
        area([days(pre(end))+0.5 days(post)-0.5], [24.5 24.5], 0.5, 'FaceColor', cast_color, 'LineStyle', 'none','showbaseline','off');
        ha = errorbar(days,Y,E,'LineWidth',2,'color','k');

        load([pegboard_dir subject '_pegs_left.mat'],'pegs','pre','post','dates');
        
        % remove any tiem points prior to pre(1)
        pegs(1:(pre(1)-1),:)=[];
        dates(1:(pre(1)-1))=[];
        post=post-pre(1)+1;
        pre=pre-pre(1)+1;
        Y=nanmean(pegs,2);
        E=[];
        for i=1:length(Y)
            if (~isnan(Y(i)))
                E(i)=std(pegs(i,~isnan(pegs(i,:))))/sqrt(sum(~isnan(pegs(i,:))));
            else
                E(i)=NaN;
            end
        end

        skip=isnan(Y);
        Y(skip)=[];
        E(skip)=[];
        ha = errorbar(days,Y,E,'LineWidth',2,'color',[.5 .5 .5],'linestyle','--');
        axis([0 70 0 25])
   end
   
   % plot results from control experiment
   if cast_onoff_figs
       % L-SM1 : R-SM1 FC
       load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat')
       roi1=1; % L-SM1 (1 = L-SM1, 2 = R-SM1, 3/4 = L/R-SMA, 5/6 = L/R-Cblm)
       roi1=roi1+6; % Hand (0 = Tongue, 6 = Hand, 12 = Foot)
       roi2=2+6;
       data=squeeze(roi_corrmat(roi1,roi2,:));
       bars_to_color=2;
       range=[-0.4 1];
       show_errorbars=True;
       show_dots=True;
       cast_bargraph_DJN({data(off_scannums), data(on_scannums)},cast_color,bars_to_color,range,show_errorbars,show_dots);
       
       % R-Cblm : L-Cblm FC
       roi1=5+6;
       roi2=6+6;
       data=squeeze(roi_corrmat(roi1,roi2,:));
       range=[-0.2 1];
       cast_bargraph_DJN({data(off_scannums), data(on_scannums)},cast_color,bars_to_color,range,show_errorbars,show_dots);
       
       % pulses
       file_suffix='_onoff';
       load([detection_outdir subject '_pusle_list.mat'],'pulse_count')
       range=[0 30];
       cast_bargraph_DJN({pulse_count(off_scannums), pulse_count(on_scannums)},cast_color,bars_to_color,range,show_errorbars,show_dots);
   end
   
   % plot pressure bladder traces and corresponding ROI time series
   if hand_movement_figs
       if strcmp(subject,'SIC03')
            hand_movements_20191121
       end
   end
end
