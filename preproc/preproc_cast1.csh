#!/bin/csh
###############
# DJN, 07/2019
###############
set basedir = /data/nil-bluearc/GMT/Dillan/Cast_Repeat/
set REFDIR = /data/petsun43/data1/atlas/
set ref = /data/petsun43/data1/atlas/TRIO_Y_NDC/
set scriptdir = /data/nil-bluearc/GMT/Dillan/scripts/cast-induced-plasticity/preproc_functions/
set subject = cast1

set subdir = $basedir/$subject/
set origdir = /data/nil-bluearc/GMT/Dillan/Cast_Paper/BIDS/
set instruction_file = $origdir/sub-$subject/${subject}_trio.params
set seslist = $origdir/sub-$subject/sub-${subject}_sessions.tsv
set sesnums = (`cat $seslist | awk 'NR>1 {print $1}'`)
echo $sesnums

#####
#goto T1_PROC
#goto T2_PROC
#goto FUNC_SORT
#goto ATLAS_LINKS
#goto GENERIC_PREPROCESS
#goto RUN_DVAR_4dfp
#goto APPLY_REGISTER_UWRP_FIRST_SESSION
#goto FC_GOOD_SURF
#####

T1_PROC:
###############
# Register T1 to atlas, debias, and average
###############
mkdir $basedir
mkdir $subdir
set structdir = ${subdir}/T1
mkdir $structdir
pushd ${structdir}

set k = 1
set h = 1
set T1 = ''
while ($k <= $#sesnums)
	echo "$origdir/sub-$subject/$sesnums[$k]/anat/*T1w.nii.gz"
	set sesT1s = (`ls -d $origdir/sub-$subject/$sesnums[$k]/anat/*T1w.nii.gz`)
	set j = 1
	while ( $j <= $#sesT1s )
		cp -sp $sesT1s[$j] ${subject}_mpr${h}.nii.gz
		set T1 = `echo $T1 ${subject}_mpr${h}.nii.gz`
		@ j++
		@ h++
	end
	@ k++
end
set T1num = $#T1
echo $T1
echo $T1num

set k = 1
while ($k <= $T1num)
	fslreorient2std ${subject}_mpr${k}.nii.gz ${subject}_mpr${k}T.nii.gz
	@ k++
end

# Debias and convert back to 4dfp
set k = 1
while ( $k <= $T1num )
	echo ${scriptdir}/apply_debias.csh ${subject}_mpr${k}T
	${scriptdir}/apply_debias.csh ${subject}_mpr${k}T
	niftigz_4dfp -4 ${subject}_mpr${k}T_debias ${subject}_mpr${k}T_debias -N
	@ k++
end

# Mask first T1 for registration	
echo bet2 ${subject}_mpr1T_debias ${subject}_mpr1T_debias_bet
bet2 ${subject}_mpr1T_debias.nii.gz ${subject}_mpr1T_debias_bet
niftigz_4dfp -4 ${subject}_mpr1T_debias_bet ${subject}_mpr1T_debias_bet -N

# Register first T1 to atlas
set modes	= (0 0 0 0 0)
@ modes[1]	= 1024 + 256 + 3
@ modes[2]	= 1024 + 256 + 3
@ modes[3]	= 3072 + 256 + 7
@ modes[4]	= 2048 + 256 + 7
@ modes[5]	= 2048 + 256 + 7
set t4file = ${subject}_mpr1T_to_TRIO_Y_NDC_t4
set mpr = ${subject}_mpr1T_debias
set mpr_mask = ${subject}_mpr1T_debias_bet
set log = ${subject}_mpr1T_to_TRIO_Y_NDC.log
@ k = 1
while ( $k <= $#modes )
	imgreg_4dfp $ref none $mpr $mpr_mask $t4file $modes[$k] >> $log
	@ k++
end

# Average T1s
set k = 1
set T1scans = ()
while ( $k <= $T1num )
	set T1scans = ( ${T1scans} ${subject}_mpr${k}T_debias )
	@ k++
end
avgmpr_4dfp ${T1scans} ${subject}_mpr_debias_avgT useold -T/data/petsun43/data1/atlas/TRIO_Y_NDC
t4imgs_4dfp -s ${subject}_mpr_debias_avgT.lst ${subject}_mpr_debias_avgT -O${subject}_mpr1T_debias

#exit

T2_PROC:
###############
# Register T2 to T1 and average
###############
set structdir = ${basedir}/${subject}/T2
mkdir $structdir
pushd ${structdir}

set k = 1
set h = 1
set T2 = ''
while ($k <= $#sesnums)
	set sesT2s = (`ls -d $origdir/sub-$subject/$sesnums[$k]/anat/*T1w.nii.gz`)
	set j = 1
	while ( $j <= $#sesT2s )
		cp -sp $sesT2s[$j] ${subject}_t2w${h}.nii.gz
		set T2 = `echo $T2 ${subject}_t2w${h}.nii.gz`
		@ j++
		@ h++
	end
	@ k++
end
set T2num = $#T2
echo $T2
echo $T2num

set k = 1
while ($k <= $T2num)
	fslreorient2std ${subject}_t2w${k}.nii.gz ${subject}_t2w${k}T.nii.gz
	@ k++
end

ln -s ../T1/${subject}_mpr1T_debias_to_TRIO_Y_NDC_t4 .
foreach e ( img img.rec ifh hdr )
	ln -s ../T1/${subject}_mpr1T_debias.4dfp.$e .
end

# Debias and convert to 4dfp
set k = 1
while ( $k <= $T2num )
	echo ${scriptdir}/apply_debias.csh ${subject}_t2w${k}T
	${scriptdir}/apply_debias.csh ${subject}_t2w${k}T
	niftigz_4dfp -4 ${subject}_t2w${k}T_debias ${subject}_t2w${k}T_debias -N
	@ k++
end

# Register T2 to T1
t2w2mpr_4dfp ${subject}_mpr1T_debias ${subject}_t2w1T_debias -T/data/petsun43/data1/atlas/TRIO_Y_NDC
set k = 1
set T2scans = ()
while ( $k <= $T2num )
	set T2scans = ( ${T2scans} ${subject}_t2w${k}T_debias )
	@ k++
end
avgmpr_4dfp ${T2scans} ${subject}_t2w_debias_avgT useold -T/data/petsun43/data1/atlas/TRIO_Y_NDC
t4imgs_4dfp -s ${subject}_t2w_debias_avgT.lst ${subject}_t2w_debias_avgT -O${subject}_t2w1T_debias
cp ${subject}_t2w1T_debias_to_TRIO_Y_NDC_t4 ${subject}_t2w_debias_avgT_to_TRIO_Y_NDC_t4
cp ${subject}_t2w1T_debias_to_${subject}_mpr1T_debias_t4 ${subject}_t2w_debias_avgT_to_${subject}_mpr1T_debias_t4
t4img_4dfp ${subject}_t2w_debias_avgT_to_${subject}_mpr1T_debias_t4 ${subject}_t2w_debias_avgT ${subject}_t2w_debias_avgT_on_${subject}_mpr1T_debias -O${subject}_mpr1T_debias.4dfp.ifh

# Create masked T2
niftigz_4dfp -n ${subject}_t2w_debias_avgT ${subject}_t2w_debias_avgT
bet2 ${subject}_t2w_debias_avgT ${subject}_t2w_debias_avgT_bet
niftigz_4dfp -4 ${subject}_t2w_debias_avgT_bet ${subject}_t2w_debias_avgT_bet
#exit


FUNC_SORT:
#############################################
# Organize functional data + make params file
#############################################
foreach k ($sesnums)
	if (! -d $subdir/$k) mkdir $subdir/$k
	pushd $subdir/$k
	set runtypes 	= ( rest motor  )
	set runs 	= (   1  motor  )
	set rest 	= (   1    0    )
	set irun_label =
	set fstd =
	set fcbolds =
	set matsizes =
	set i = 1
	while ($i <= $#runtypes)
		set funcs = `ls -d $origdir/sub-$subject/$k/func/*task-$runtypes[$i]*.nii.gz`
		set j = 1
		while ($j <= $#funcs)
			if ($#funcs > 1) then
				set run = $runs[$i]$j
			else
				set run = $runs[$i]
			endif
			mkdir bold$run
			#niftigz_4dfp -4 $funcs[$j] bold$run/study${i}
			set irun_label = `echo $irun_label $run`
			set fstd = `echo $fstd $i`
			set filename = `echo $funcs[$j]:r:r`
			set matsize = `jq '.ReconMatrixPE' ${filename}.json`
			set matsizes = `echo $matsizes $matsize`
			set seqstr = `jq '.SliceTiming' ${filename}.json | head -15 | tail -14 | gawk '{print NR, $1}' | sort -n -k 2,2 | gawk '{printf("%d,", $1);}'`
			if ($rest[$i]) set fcbolds = `echo $fcbolds $run`
			@ j++
		end
		@ i++
	end

	# link field maps
	mkdir sefm
	set fmaps = `ls -d $origdir/sub-$subject/$k/fmap/*gz`
	set sefm = 
	set j = 1
	while ( $j <= $#fmaps )
		cp -sp $fmaps[$j] sefm/${k}_sefm${j}.nii.gz
		cp -sp ${fmaps[$j]:r:r}.json sefm/${k}_sefm${j}.json
		set sefm = `echo $sefm $j`
		@ j++
	end
		
	# write params file
	echo "set subject  = $subject" > $k.params
	echo "set patid    = $k" >> $k.params
	echo "set patid1   = $k" >> $k.params
	echo "set E4dfp    = 1" >> $k.params
	echo "set irun     = ($irun_label)" >> $k.params
	echo "set fstd     = ($fstd)" >> $k.params
	echo "set matrix   = ($matsizes)" >> $k.params
	#echo "set nounpack = 1" >> $k.params # check this with Avi
	echo "set seqstr   = " $seqstr >> $k.params
	echo "set sefm 	   = ($sefm)" >> $k.params
	echo "set fcbolds = ($fcbolds)" >> $k.params
	popd
end
#exit

ATLAS_LINKS:
####################
# create atlas links
####################
foreach k ($sesnums)
	pushd $subdir/$k
	mkdir atlas
	pushd atlas
	ln -sf ${subdir}/T1/${subject}_mpr1T_debias_to_TRIO_Y_NDC_t4 ./${k}_mpr1_to_TRIO_Y_NDC_t4
	ln -sf ${subdir}/T2/${subject}_t2w_debias_avgT_to_${subject}_mpr1T_debias_t4 ./${k}_t2w_to_${k}_mpr1_t4
	ln -sf ${subdir}/T2/${subject}_t2w_debias_avgT_to_TRIO_Y_NDC_t4 ./${k}_t2w_to_TRIO_Y_NDC_t4
	foreach e ( img ifh img.rec hdr )
		ln -sf ${subdir}/T1/${subject}_mpr_debias_avgT_111_t88.4dfp.$e ./${k}_mpr_n1_111_t88.4dfp.$e
		ln -sf ${subdir}/T1/${subject}_mpr_debias_avgT_222_t88.4dfp.$e ./${k}_mpr_n1_222_t88.4dfp.$e
		ln -sf ${subdir}/T1/${subject}_mpr_debias_avgT_333_t88.4dfp.$e ./${k}_mpr_n1_333_t88.4dfp.$e
		ln -sf ${subdir}/T2/${subject}_t2w_debias_avgT_111_t88.4dfp.$e ./${k}_t2w_111_t88.4dfp.$e
		ln -sf ${subdir}/T2/${subject}_t2w_debias_avgT_222_t88.4dfp.$e ./${k}_t2w_222_t88.4dfp.$e
		ln -sf ${subdir}/T2/${subject}_t2w_debias_avgT_333_t88.4dfp.$e ./${k}_t2w_333_t88.4dfp.$e
		ln -sf ${subdir}/T1/${subject}_mpr_debias_avgT.4dfp.$e ./${k}_mpr1.4dfp.$e
		ln -sf ${subdir}/T2/${subject}_t2w_debias_avgT.4dfp.$e ./${k}_t2w.4dfp.$e
	end
	popd	
	popd	
end
#exit

GENERIC_PREPROCESS:
###############################################
# Generic preprocessing for dcm_to_4dfp etc...
###############################################
foreach k ( $sesnums )
	pushd $subdir/$k
	$scriptdir/cross_bold_dn_180706.csh ${k}.params $instruction_file $scriptdir
	popd
end
#exit

RUN_DVAR_4dfp:
#######################################
# run_dvar_4dfp individually on each run
#######################################

foreach k ( $sesnums )

	set patid = $k
	pushd ${subdir}/${patid}
	source ${patid}.params
	foreach	r ( $irun )
		pushd ./bold$r/
		echo ${patid}_b${r}_faln_dbnd_xr3d_norm > ${patid}_b${r}.lst
		conc_4dfp ${patid}_b${r}_faln_dbnd_xr3d_norm -l${patid}_b${r}.lst
		run_dvar_4dfp ${patid}_b${r}_faln_dbnd_xr3d_norm.conc -m../atlas/${patid}_func_vols_ave -n0 -b10 -x8
		popd
	end
	popd
end
#exit

APPLY_REGISTER_UWRP_FIRST_SESSION:
###########################################################
# Register func vol ave unwarp to first session epi and resample bold data
###########################################################
set T_epi      = ${subdir}/$sesnums[1]/unwarp/$sesnums[1]_func_vols_ave_uwrp
set T_epi_mask = ${subdir}/$sesnums[1]/unwarp/$sesnums[1]_func_vols_ave_uwrp_mskt
set U          = ${subdir}/$sesnums[1]/unwarp/$sesnums[1]_func_vols_ave_uwrp_to_TRIO_Y_NDC_t4
source $instruction_file

# generate mask for first session
pushd ${subdir}/$sesnums[1]/unwarp
echo msktgen_4dfp $sesnums[1]_func_vols_ave_uwrp_mskt -T$REFDIR/TRIO_Y_NDC
msktgen_4dfp $sesnums[1]_func_vols_ave_uwrp -T$REFDIR/TRIO_Y_NDC
popd

# remake single resampled 333 atlas space fMRI volumetric timeseries for first session
set patid = $sesnums[1]
pushd ${subdir}/${patid}
source ${patid}.params
$rsam_cmnd ${patid}.params $instruction_file

set MBstr = _faln_dbnd
set lst = ${patid}${MBstr}_xr3d_uwrp_atl.lst
if (-e $lst) /bin/rm $lst
touch $lst
@ k = 1
while ($k <= $#irun)
	echo bold$irun[$k]/${patid}_b$irun[$k]${MBstr}_xr3d_uwrp_atl.4dfp.img >> $lst
	@ k++
end
conc_4dfp ${lst:r}.conc -l$lst
if ($status) exit $status
set format = atlas/${patid}_func_vols.format
if ($status) exit $status
actmapf_4dfp $format	${patid}${MBstr}_xr3d_uwrp_atl.conc -aave
if ($status) exit $status
ifh2hdr -r2000 		${patid}${MBstr}_xr3d_uwrp_atl_ave
mv			${patid}${MBstr}_xr3d_uwrp_atl_ave.4dfp.*	unwarp
var_4dfp -sf$format	${patid}${MBstr}_xr3d_uwrp_atl.conc
ifh2hdr -r20		${patid}${MBstr}_xr3d_uwrp_atl_sd1
mv			${patid}${MBstr}_xr3d_uwrp_atl_sd1*		unwarp
mv			${patid}${MBstr}_xr3d_uwrp_atl.conc*		unwarp

# Register epi to first session epi, and resample BOLD to atlas
set modes = (0 0 0 0)
@ modes[1] = 2048 + 3 + 256 
@ modes[2] = 2048 + 3 + 256 + 4
@ modes[3] = 2048 + 3 + 256 + 4
@ modes[4] = $modes[3]

@ n = $#sesnums
@ i = 2
while ( $i <= $n )
	set patid = $sesnums[$i]
	cd ${subdir}/${patid}
	source ${patid}.params
	pushd unwarp	# into unwarp
	set t4file = ${patid}_func_vols_ave_uwrp_to_$sesnums[1]_func_vols_ave_uwrp_t4
	if ($status) exit $status
	set log =    ${patid}_func_vols_ave_uwrp_to_$sesnums[1]_func_vols_ave_uwrp.log
	date >! $log
	@ k = 1
	while ($k <= $#modes)
	echo	imgreg_4dfp ${T_epi} ${T_epi_mask} ${patid}_func_vols_ave_uwrp none $t4file $modes[$k] >> $log
		imgreg_4dfp ${T_epi} ${T_epi_mask} ${patid}_func_vols_ave_uwrp none $t4file $modes[$k] >> $log
		if ($status) exit $status
		@ k++
	end
	t4_mul $t4file $U ${patid}_func_vols_ave_uwrp_to_TRIO_Y_NDC_t4
	t4img_4dfp ${patid}_func_vols_ave_uwrp_to_TRIO_Y_NDC_t4 ${patid}_func_vols_ave_uwrp ${patid}_func_vols_ave_uwrp_on_TRIO_Y_NDC_111 -O111
	t4img_4dfp ${patid}_func_vols_ave_uwrp_to_TRIO_Y_NDC_t4 ${patid}_func_vols_ave_uwrp ${patid}_func_vols_ave_uwrp_on_TRIO_Y_NDC_333 -O333
	if ($status) exit $status
	popd		# out of unwarp
	$rsam_cmnd ${patid}.params $instruction_file

	# remake single resampled 333 atlas space fMRI volumetric timeseries	
	set MBstr = _faln_dbnd
	set lst = ${patid}${MBstr}_xr3d_uwrp_atl.lst
	if (-e $lst) /bin/rm $lst
	touch $lst
	@ k = 1
	while ($k <= $#irun)
		echo bold$irun[$k]/${patid}_b$irun[$k]${MBstr}_xr3d_uwrp_atl.4dfp.img >> $lst
		@ k++
	end
	conc_4dfp ${lst:r}.conc -l$lst
	if ($status) exit $status
	set format = atlas/${patid}_func_vols.format
	if ($status) exit $status
	actmapf_4dfp $format	${patid}${MBstr}_xr3d_uwrp_atl.conc -aave
	if ($status) exit $status
	ifh2hdr -r2000 		${patid}${MBstr}_xr3d_uwrp_atl_ave
	mv			${patid}${MBstr}_xr3d_uwrp_atl_ave.4dfp.*	unwarp
	var_4dfp -sf$format	${patid}${MBstr}_xr3d_uwrp_atl.conc
	ifh2hdr -r20		${patid}${MBstr}_xr3d_uwrp_atl_sd1
	mv			${patid}${MBstr}_xr3d_uwrp_atl_sd1*		unwarp
	mv			${patid}${MBstr}_xr3d_uwrp_atl.conc*		unwarp

	@ i++
end
#exit

FC_GOOD_SURF:
###########################################
# combined good_voxels + surface projection
###########################################
foreach patid ($sesnums)
	pushd $patid
	source ${patid}.params

	# FC proc
	echo $scriptdir/fcMRI_preproc_180730.csh $patid.params $instruction_file
	$scriptdir/fcMRI_preproc_180730.csh $patid.params $instruction_file
	if ($status) exit $status

	# good voxels
	echo $patid > temp_patid.txt
	$scriptdir/create_goodvoxels_mask_mod.csh ${subject} `pwd`/temp_patid.txt $subdir/7112b_fs_LR/Ribbon $subdir
	rm temp_patid.txt

	# tasks
	@ k = 2
	while ($k <= $#irun)
		set run = $irun[$k]
		pushd bold$run	
		$scriptdir/create_cifti_goodvoxels_SIC.csh $subject $patid ${patid}_b${run}_faln_dbnd_xr3d_uwrp_atl
		popd
		@ k++
	end

	#rest
	pushd bold1
	$scriptdir/create_cifti_goodvoxels_SIC.csh $subject $patid ${patid}_b1_faln_dbnd_xr3d_uwrp_atl_bpss_resid
	popd
	popd
end
