settings_dir='/Users/newboldd/data/Cast_Paper/';

subjects = {'sub-cast1', 'sub-cast2', 'sub-cast3'};
subjects_to_run = [1 2 3];

% Analyses to run
run_accelerometry           = 0;
run_task_analysis           = 1;
run_motor_rois              = 1;
run_tmasks                  = 1;
run_roi_timecourses         = 1;
run_parcel_corrmat          = 1;
run_select_parcels_1        = 1;
run_select_parcels_2        = 0;
run_fourier                 = 1;
run_detection               = 1;
run_anova                   = 1;
run_propagation             = 1;
run_movement                = 1;
run_tmask_onoff             = 1;
run_roi_corrmat_onoff       = 1;
run_detection_onoff         = 1;

% loop through subjects
for s=subjects_to_run
    subject = subjects{s};
    disp(subject)
    load([settings_dir subject '_settings.mat'])

    % accelerometry
    if run_accelerometry
        if (exist(accel_outdir,'dir')~=7)
            system(['mkdir ' accel_outdir])
        end
        [lu,ru,N]=accel_use_counts_DJN(accel_outdir);
        save([accel_outdir subject '_use_counts.mat'],'lu','ru','N','accel_cast_period');
    end

    % task analysis
    if run_task_analysis
        if (exist(task_outdir,'dir')~=7)
            system(['mkdir ' task_outdir])
        end
        if (exist([task_outdir 'timecourses'],'dir')~=7)
            system(['mkdir ' task_outdir 'timecourses']);
        end
        task_timecourses_DJN(subject,TR,BIDS_dir,task_outdir)
        timecourse_consistency_DJN(subject,task_outdir)
    end

   % roi selection
   if run_motor_rois
       disp('finding activiation peaks...')
       select_rois_DJN(subject,[task_outdir 'sync/'],fsdir,roi_outdir, labeltype) % need to split cerebellum into sup and inf halves to fix SIC03
       disp('growing ROIs...')
       grow_rois_DJN(subject, task_outdir, fsdir, roi_outdir, labeltype)
   end

   % read tmasks
   if run_tmasks
       if (exist(tmask_outdir,'dir')~=7)
            system(['mkdir ' tmask_outdir])
       end
       disp('reading tmasks')
       rest_dir = [BIDS_dir 'derivatives/surface/' subject 'processed_restingstate_timecourses/'];
       sessions=tdfread(rest_sesslist);
       sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'pre','cast','post'}));
       tmask=zeros(length(sessions),rest_frames);
       for i=1:length(sessions)
           disp([num2str(i) ': ' sessions(i,:)])
           tmask_name = [rest_dir sessions(i,:) 'cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR_tmask.txt'];
           fid=fopen(tmask_name,'r');
           tmask(i,:)=fscanf(fid,'%f')';
           fclose(fid);
       end
       tmask=logical(tmask);
       save([tmask_outdir subject '_tmask.mat'],'tmask');
   end

   % roi correlation matrix
   if run_roi_timecourses
       disp('making ROI corrmats')
       rest_dir = [BIDS_dir 'derivatives/surface/' subject 'processed_restingstate_timecourses/'];
       load([tmask_outdir subject '_tmask'],'tmask');
       tongue_rois=ft_read_cifti_mod([roi_outdir subject '_Tongue_ROIs.dtseries.nii']);
       hand_rois=ft_read_cifti_mod([roi_outdir subject '_Hand_ROIs.dtseries.nii']);
       foot_rois=ft_read_cifti_mod([roi_outdir subject '_Foot_ROIs.dtseries.nii']);
       load([tmask_outdir subject '_tmask.mat'],'tmask');
       sessions=tdfread(rest_sesslist);
       sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'pre','cast','post'}));
       roi_time=zeros(18,rest_frames,length(sessions));
       roi_corrmat=zeros(18,18,length(sessions));
       for i=1:length(sessions)
           disp(sessions(i,:))
           time=ft_read_cifti_mod([rest_dir sessions(i,:) '/cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR.dtseries.nii']);
           assert(size(time.data,1)==size(hand_rois.data,1),'Error: ROI size != bold size')
           for j=1:6
               roi_time(j,:,i)=mean(time.data(tongue_rois.data==j,:),1);
               roi_time(j+6,:,i)=mean(time.data(hand_rois.data==j,:),1);
               roi_time(j+12,:,i)=mean(time.data(foot_rois.data==j,:),1);
           end
           roi_corrmat(:,:,i)=corr(roi_time(:,tmask(i,:),i)');
       end
       save([roi_outdir subject '_ROI_timecourses.mat'],'roi_time');
       save([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
   end
   
   % parcel correlation matrix
   if run_parcel_corrmat
       disp('extracting parcel time courses')
       if (exist(parcel_outdir,'dir')~=7)
            mkdir(parcel_outdir);
       end
       
       % load tmask + sessions
       load([tmask_outdir subject '_tmask.mat'],'tmask')
       sessions=tdfread(rest_sesslist);
       sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'pre','cast','post'}));
       
       % load surface parcellation
       parcel_path=[BIDS_dir 'derivatives/surface/' subject '/surface_parcellation/' subject '_parcels.dtseries.nii'];
       parcels=ft_read_cifti_mod(parcel_path);
       
       % read community list
       fid=fopen(community_list);
       communities=fscanf(fid,'%f');
       fclose(fid);
       communities(communities>17)=0;
       
       % sort parcels by hemisphere
       parcel_list=unique(parcels.data(parcels.data>0));
       struct=parcels.brainstructure(parcels.brainstructure>0);
       clear hem_list
       for i=1:length(parcel_list)
           hem_list(i)=mode(struct(parcels.data==parcel_list(i)));
       end
       [~,sorti]=sort(hem_list);
       parcel_list=parcel_list(sorti);
       communities=communities(sorti);
       
       % extract time series
       rest_dir = [BIDS_dir 'derivatives/surface/' subject 'processed_restingstate_timecourses/'];
       clear parcel_time
       clear parcel_corrmat
       for i=1:length(sessions)
           disp([num2str(i) ': ' sessions(i,:)])
           time=ft_read_cifti_mod([rest_dir sessions(i,:) '/cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR.dtseries.nii']);
           for j=1:length(parcel_list)
               parcel_time(j,:,i)=mean(time.data(parcels.data==parcel_list(j),:),1);
           end
           parcel_corrmat(:,:,i)=corr(parcel_time(:,tmask(i,:),i)');
       end
       
       save([parcel_outdir subject '_parcel_time.mat'],'parcel_time','communities');
       save([parcel_outdir subject '_parcel_corrmat.mat'],'parcel_corrmat','communities');
   end
   
   % primary motor parcel selection
   if run_select_parcels_1
       disp('making motor parcel files for editing')
       parcel_path=[BIDS_dir 'derivatives/surface/' subject '/surface_parcellation/' subject '_parcels.dtseries.nii'];
       load([parcel_outdir subject '_parcel_time.mat'],'communities');
       primary_somatomotor_parcels_part1(subject,communities,parcel_path,fsdir,labeltype,parcel_outdir,sort_parcels)
   end

   if run_select_parcels_2
       disp('removing problem parcels')
       % bad_parcels: parcels that do not clearly belong to their respecitive sub-networks
       % fill out after running select_parcels_1
       %
       % each cell position represents 1 hemisphere of 1 sub-network:
       % LH-SMN-ue, RH-SMN-ue, LH-SMN-face, RH-SMN-face, LH-SMN-le, RH-SMN-le
       % fill in eany position with a list of parcels that were misassigned to the the corresponding subnetwork
       %
       % parcel indicies come from cifti file located at:
       % [parcel_outdir subject '_somatomotor_parcels_for_editing.dtseries.nii']

       switch subject
           case 'cast1'
                bad_parcels={[],[],[],[],[],[10 11]};
           case 'cast2'
               bad_parcels={[],[],[3],[],[],[]};
           case 'cast3'
               bad_parcels={[1 4],[1 5 6 7 8],[1 2 9],[1 6],[],[]};
       end
       parcel_path=[BIDS_dir 'derivatives/surface/' subject '/surface_parcellation/' subject '_parcels.dtseries.nii'];
       primary_somatomotor_parcels_part2(subject,communities,parcel_path,fsdir,labeltype,parcel_outdir,bad_parcels,sort_parcels)
   end

   % Fourier transform
   if(run_fourier)
      disp('Running Fourier transform')
      smoothness=30;
      if (exist(spectra_outdir,'dir')~=7)
          system(['mkdir ' spectra_outdir])
      end
      tongue_rois=ft_read_cifti_mod([roi_dir subject '_Tongue_ROIs.dtseries.nii']);
      hand_rois=ft_read_cifti_mod([roi_dir subject '_Hand_ROIs.dtseries.nii']);
      foot_rois=ft_read_cifti_mod([roi_dir subject '_Foot_ROIs.dtseries.nii']);
      sessions=tdfread(rest_sesslist);
      sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'pre','cast','post'}));
      roi_spec=zeros(length(sessions),rest_frames/2+1,18);
      rest_dir = [BIDS_dir 'derivatives/surface/' subject 'processed_restingstate_timecourses/'];
      for i=1:length(sessions)
          disp([num2str(i) ': ' sessions(i,:)])
          time=ft_read_cifti_mod([rest_dir sessions(i,:) '/cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR.dtseries.nii']);
          P2=abs(fft(time.data')');
          P1=P2(:,1:rest_frames/2+1);
          P1(:,2:end-1)=2*P1(:,2:end-1);
          global_spec(i,:)=mean(P1,1);
          assert(size(time.data,1)==size(hand_rois.data,1),'Error: ROI size != bold size')
          for j=1:6
              roi_spec(i,:,j)=mean(P1(tongue_rois.data==j,:),1);
              roi_spec(i,:,j+6)=mean(P1(hand_rois.data==j,:),1);
              roi_spec(i,:,j+12)=mean(P1(foot_rois.data==j,:),1);
          end
       end

       % ROI spectra
       roi_spec_smooth=zeros(size(roi_spec));
       representations = {'tongue','hand','foot'};
       hemispheres = {'l','r'};
       regions={'M1','SMA','Cblm'};
       for rep=1:3
           for hem=1:2
               for reg=1:3
                   % smooth ROI spectrum
                   roi_num = (rep-1)*6 + (reg-1)*2 + hem;
                   spec=roi_spec(:,:,roi_num);
                   f = (0:size(roi_spec,2)-1)/rest_frames/TR;
                   for i=1:size(spec,1)
                       spec(i,:)=smooth(spec(i,:),smoothness);
                   end
                   roi_spec_smooth(:,:,roi_num)=spec;
               end
           end
       end
       save([spectra_outdir subject '_spectra.mat'],'roi_spec','roi_spec_smooth','representations','regions')
   end

   % Pulse detection
   if(run_detection)
       disp('Detecting pulses')
       if (exist(detection_outdir,'dir')~=7)
           system(['mkdir ' detection_outdir])
       end

       tmask_spread=3; % ignore frames adjacent to censored frames
       roi1=1+6;
       roi2=2+6;
       thresh1=4; % threshold for L-SM1ue
       thresh2=3; % threshold for difference between L-SM1ue and R-SM1ue

       file_suffix='';
       load([tmask_outdir subject '_tmask' file_suffix '.mat'],'tmask')
       load([roi_dir subject '_ROI_timecourses' file_suffix '.mat'],'roi_time')

       plot_pulse_locations=true;
       plot_pulse_counts=true;
       pulse_detection(tmask_spread, tmask, roi_time, roi1, roi2, thresh1, thresh2, plot_pulse_locations, plot_pulse_counts, file_suffix)
   end

   % Activation mapping ( ANOVA )
   if (run_anova)
       disp('Running pulse ANOVA')
       if (exist(map_outdir,'dir')~=7)
           system(['mkdir ' map_outdir])
       end
       sessions=tdfread(rest_sesslist);
       sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'pre','cast','post'}));
       rest_dir = [BIDS_dir 'derivatives/surface/' subject 'processed_restingstate_timecourses/'];
       if (exist([map_outdir subject '_pulse_time.mat'])==2)
           disp('Loading pulse_time')
           load([map_outdir subject '_pulse_time.mat'],'pulse_time')
           temp=ft_read_cifti_mod([rest_dir sessions(i,:) '/cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR.dtseries.nii']);
       else
           disp('Extracting pulse time courses')
           load([detection_outdir subject '_pulse_list.mat'])
           clear pulse_time
           k=1;
           lead=6*2.2/TR; % so the different TRs cover the same amount of time
           lag=8*2.2/TR;
           for i=1:length(sessions)
               if pulse_count(i)>0
                   disp([num2str(i) ': ' sessions{i}])
                   temp=ft_read_cifti_mod([rest_dir sessions(i,:) '/cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR.dtseries.nii']);
                   time=[NaN(size(temp.data,1),lead) temp.data NaN(size(temp.data,1),lag)];
                   for j=1:length(pulse_list{i})
                       x=pulse_list{i}(j);
                       pulse_time(:,:,k)=time(:,x:x+lead+lag);
                       k=k+1;
                   end
               end
           end
           pulse_time=pulse_time/10; % % signal change (mode-1000 normalized)
           save([map_outdir subject '_pulse_time.mat'],'pulse_time','pulse_list','pulse_count','-V7.3')
           temp.data=nanmean(pulse_time,3);
           ft_write_cifti_mod([map_outdir subject '_mean_pulse_time.dtseries.nii'],temp);
       end
       disp('Computing ANOVA')
       temp.data=task_anova_DJN(pulse_time);
       ft_write_cifti_mod([map_outdir subject '_pulse_ANOVA.dtseries.nii'],temp);
   end

   % Pulse propagation
   if(run_propagation)
       disp('Analyzing pulse propagation')
       load([tmask_outdir subject '_tmask.mat'],'tmask')
       load([detection_outdir subj '_pulse_list.mat'],'pulse_list','pulse_count')
       load([roi_outdir subj '_ROI_timecourses.mat'],'roi_time');

       roi1=1; % L-SM1 (1 = L-SM1, 2 = R-SM1, 3/4 = L/R-SMA, 5/6 = L/R-Cblm)
       roi1=roi1+6; % Hand (0 = Tongue, 6 = Hand, 12 = Foot)
       rois=[3 1 6]+6;
       roi_names={'SMA','SM1','Cblm'};
       reference_roi=2; % which ROI in "rois" should be used as a zero point for pulse timing
       plot_timecourses=false;
       plot_time_delays=false;

       pulse_propagation(tmask, pulse_list, pulse_count, roi_time, TR, subject, plot_timecourses, rois, roi_names, reference_roi, plot_time_delays)
   end

   % Hand movement analysis
   if(run_movement)
       disp('Analyzing hand movements')
       movement_thresh=0.03;

       % specify points of sudden amplitdue change
       amplitude_change=nan(10,1);
       amplitude_change(3)=253406;
       amplitude_change(6)=246627;

       sessions=tdfread(rest_sesslist);
       bold_list=sessions.SESSION(strcmp(sessions.CONDITION,{'pre','cast','post'}));
       load([detection_outdir subject '_pulse_list.mat'], 'pulse_list', 'pulse_count')
       load([roi_outdir subj '_ROI_timecourses.mat'],'roi_time');
       load([tmask_outdir subject '_tmask.mat'],'tmask')

       hand_movements_DJN(movement_thresh, amplitude_change, movedir, bold_list, pulse_list, pulse_count, roi_time, tmask, cast_color)
   end

   % read tmasks onoff
   if run_tmask_onoff
       disp('reading tmasks')
       rest_dir = [BIDS_dir 'derivatives/surface/' subject 'processed_restingstate_timecourses/'];
       sessions=tdfread(rest_sesslist);
       sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'on','off'}));
       tmask=zeros(length(sessions),rest_frames);
       for i=1:length(sessions)
           disp([num2str(i) ': ' sessions(i,:)])
           tmask_name = [rest_dir sessions(i,:) 'cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR_tmask.txt'];
           fid=fopen(tmask_name,'r');
           tmask(i,:)=fscanf(fid,'%f')';
           fclose(fid);
       end
       tmask=logical(tmask);
       save([tmask_outdir subject '_tmask_onoff.mat'],'tmask');
   end

   % ROI time courses onoff
   if run_roi_corrmat_onoff
       file_suffix='_onoff';
       disp('making ROI corrmats')
       rest_dir = [BIDS_dir 'derivatives/surface/' subject 'processed_restingstate_timecourses/'];
       load([tmask_outdir subject '_tmask' file_suffix '.mat']);
       tongue_rois=ft_read_cifti_mod([roi_outdir subject '_Tongue_ROIs.dtseries.nii']);
       hand_rois=ft_read_cifti_mod([roi_outdir subject '_Hand_ROIs.dtseries.nii']);
       foot_rois=ft_read_cifti_mod([roi_outdir subject '_Foot_ROIs.dtseries.nii']);
       sessions=tdfread(rest_sesslist);
       sessions=sessions.SESSION(strcmp(sessions.CONDITION,{'on','off'}));
       roi_time=zeros(18,rest_frames,length(sessions));
       roi_corrmat=zeros(18,18,length(sessions));
       for i=1:length(sessions)
           disp([num2str(i) ': ' sessions{i}])
           time=ft_read_cifti_mod([rest_dir sessions(i,:) '/cifti/' subject '_' sessions(i,:) '_task-rest_bold_32k_fsLR.dtseries.nii']);
           assert(size(time.data,1)==size(hand_rois.data,1),'Error: ROI size != bold size')
           for j=1:6
               roi_time(j,:,i)=mean(time.data(tongue_rois.data==j,:),1);
               roi_time(j+6,:,i)=mean(time.data(hand_rois.data==j,:),1);
               roi_time(j+12,:,i)=mean(time.data(foot_rois.data==j,:),1);
           end
           roi_corrmat(:,:,i)=corr(roi_time(:,tmask(i,:),i)');
       end
       save([roi_outdir subject '_ROI_timecourses' file_suffix '.mat'],'roi_time');
       save([roi_outdir subject '_ROI_corrmat' file_suffix '.mat'],'roi_corrmat');
   end

   % Pulse detection onoff
   if(run_detection_onoff)
       file_suffix='_onoff';
       load([tmask_outdir subject '_tmask' file_suffix '.mat'],'tmask')
       load([roi_dir subject '_ROI_timecourses' file_suffix '.mat'],'roi_time')

       plot_pulse_locations=true;
       plot_pulse_counts=true;
       pulse_detection(tmask_spread, tmask, roi_time, roi1, roi2, thresh1, thresh2, plot_pulse_locations, plot_pulse_counts, file_suffix)
   end
end
