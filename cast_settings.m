clear



% Input path
BIDS_dir = '/Users/newboldd/data/cast_induced_plasticity/';

% Output path
outdir = '/Users/newboldd/data/Cast_Paper/';

% Dependencies
wb_dir = '/Users/newboldd/Projects/bin_macosx64/';



subjects = {'sub-cast1', 'sub-cast2', 'sub-cast3'};
for s=1:3
    if (exist(outdir,'dir')~=7)
        system(['mkdir ' outdir])
    end
    accel_outdir = [outdir '/accel/'];
    tmask_outdir = [outdir '/tmask/'];
    spectra_outdir = [outdir 'spectra/'];
    day_spec_outdir = [spectra_outdir 'by_session/'];
    alff_dir = [outdir 'ALFF/'];
    task_outdir = [outdir '/motor_task/'];
    roi_dir = [outdir '/rois/'];
    seedmap_outdir = [outdir '/seedmaps/'];
    ttest_outdir = [seedmap_outdir '/ttests/'];
    fcplot_ourdir = [roi_dir '/fcplots/'];
    parcel_outdir = [outdir '/parcels/'];
    spring_embed_outdir = [outdir 'spring_embed/'];
    CON_outdir = [outdir '/CON_timecourse/'];
    detection_outdir = [outdir 'detection/'];
    censor_outdir = [outdir '/censoring/'];
    regression_outdir = [outdir 'pulse_regression/'];
    map_outdir = [outdir 'activation_map/'];
    movedir=[outdir 'hand_movement_monitoring/'];
    stats_outdir = [outdir '/statistics/'];
    strength_dir = [outdir '/strength/'];
    pegboard_dir = [outdir '/pegboard/'];

    % Subject-specific variables
    subject = subjects{s};
    disp(subject)
    
    basedir = [BIDS_dir subject];
    rest_sesslist = [basedir subject '_sessions.tsv'];
    parcel_path = [BIDS_dir 'derivatives/surface/' subject '/surface_parcellation/' subject '_parcels.dtseries.nii'];
    community_list = [BIDS_dir 'derivatives/surface/' subject '/surface_parcellation/' subject '_parcel_communities.txt'];
    fsdir = [BIDS_dir 'derivatives/surface/' subject '/fs_LR_Talairach/fsaverage_LR32k/'];
    infomap=[BIDS_dir 'derivatives/surface/' subject '/cifti_networks/' subject '_networks.dscalar.nii'];

    switch subject
        case 'sub-cast1'
            cast_onset=9*24+9; % 9:00am on 10th day
            cast_offset=23*24+9; % 9:00am on 24th day
            measurement_days=[1:18 20:36 38:46 48:50];
            measurement_times=5+24*(measurement_days-1); % 5:00am each day
            TR=2.2;
            rest_frames=818;
            onoff_rest_frames=1636;
            pre_scannums=1:11;
            cast_scannums=12:24;
            post_scannums=25:48;
            on_scannums=[1 2 6 8 9 10];
            off_scannums=[3 4 5 7 11 12];
            labeltype = 'old';
            cast_color=[231 125 145]/255;
            sort_parcels=1;
        case 'sub-cast2'
            pre_scannums=1:12;
            cast_scannums=13:26;
            post_scannums=27:63;
            cast_color=[239 220 33]/255;
            TR=1.1;
            cast_onset=12*24+9; % 9:00am on 13th day
            cast_offset=26*24+9; % 9:00am on 27th day
            measurement_times=21:24:64*24; % 9:00pm each day for 64 days
            measurement_times(23)=[]; % missing one time point
            labeltype = 'new';
            rest_frames=1636;
            onoff_rest_frames=1636;
            on_scannums=[1 2 6 8 9 10 14 16 17 18 22 24];
            off_scannums=[3 4 5 7 11 12 13 15 19 20 21 23];
            sort_parcels=1;
        case 'sub-cast3'
            pre_scannums=1:14;
            cast_scannums=15:28;
            post_scannums=[30 32:44];
            cast_color=[109 197 201]/255;
            TR=1.1;
            cast_onset=14*24+9; % 9:00am on 15th day
            cast_offset=28*24+9; % 9:00am on 29th day
            measurement_times=21:24:42*24; % 9:00pm each day for 42 days
            MSCname = 'SIC03';
            rest_frames=1636;
            onoff_rest_frames=1636;
            on_scannums=[2 4 6 8 10 12];
            off_scannums=[1 3 5 7 9 11];
            labeltype = 'new';
            sort_parcels=0;
            bladder_rest_list = [basedir 'bladder_rest.txt'];
    end
    save([outdir subject '_settings.mat'])
end
