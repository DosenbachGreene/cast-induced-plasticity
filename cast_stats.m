settings_dir='/Users/newboldd/data/Cast_Paper/';

subjects = {'sub-cast1', 'sub-cast2', 'sub-cast3'};
subjects_to_run = [1 2 3];

% Tests to run
accel_ttest           = 1;
strength_ttest        = 1;
pegboard_ttest        = 1;
fc_timecourse_perm    = 1;
cast_onset_ttest      = 1;
modularity_perm       = 1;
alff_ttest            = 1;
pulse_count_ttest     = 1;
pulse_time_WSR_test   = 1;
onoff_fc_ttest        = 1;
onoff_pulse_ttest     = 1;

% load settings
load([settings_dir subjects(subjects_to_run(1)) '_settings.mat'])

if (exist(stats_outdir,'dir')~=7)
    system(['mkdir ' stats_outdir])
end

% daily use (accelerometry)
if accel_ttest
    fid=fopen([stats_outdir 'accel_ttest.txt'],'w');
    % Right UE
    fprintf(fid,'%s\n','accelerometry t-tests: right UE');
    for i=1:length(subjects)
        subject=subjects{i};
        load([accel_outdir subject '_use_counts.mat'],'lu','ru','accel_cast_period');
        pre=1:(accel_cast_period(1)-1);
        cast=accel_cast_period(1):accel_cast_period(2);
        [~,p,~,stats]=ttest2(ru(cast),ru(pre));
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'n = %d pre, %d cast\n',sum(~isnan(ru(pre))),sum(~isnan(ru(cast))));
        fprintf(fid,'df = %d\n', stats.df);
        fprintf(fid,'mean_pre = %f\n', nanmean(ru(pre)));
        fprintf(fid,'mean_cast = %f\n', nanmean(ru(cast)));
        fprintf(fid,'percent_change = %f\n', (nanmean(ru(cast))-nanmean(ru(pre)))/nanmean(ru(pre))*100);
        fprintf(fid,'t = %f\n', stats.tstat);
        fprintf(fid,'P = %f\n', p);
    end
    % Left UE
    fprintf(fid,'\n\n%s\n','accelerometry t-tests: left UE');
    for i=1:length(subjects)
        subject=subjects{i};
        load([accel_outdir subject '_use_counts.mat'],'lu','ru','accel_cast_period');
        pre=1:(accel_cast_period(1)-1);
        cast=accel_cast_period(1):accel_cast_period(2);
        [~,p,~,stats]=ttest2(lu(cast),lu(pre));
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'n = %d pre, %d cast\n',sum(~isnan(lu(pre))),sum(~isnan(lu(cast))));
        fprintf(fid,'df = %d\n', stats.df);
        fprintf(fid,'mean_pre = %f\n', nanmean(lu(pre)));
        fprintf(fid,'mean_cast = %f\n', nanmean(lu(cast)));
        fprintf(fid,'percent_change = %f\n', (nanmean(lu(cast))-nanmean(lu(pre)))/nanmean(lu(pre))*100);
        fprintf(fid,'t = %f\n', stats.tstat);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% t-test of grip strength immediately after cast removal vs. pre
if strength_ttest
    fid=fopen([stats_outdir 'strength_ttest.txt'],'w');
    % Right UE
    fprintf(fid,'%s\n','grip strength t-tests: right UE');
    for i=1:length(subjects)
        subject = subjects{i};
        load([strength_dir subject '_grip_right.mat'],'grip','pre','post');
        pre_grip=grip(pre,:);
        post_grip=grip(post,:);
        [~,p,~,stats]=ttest2(post_grip(:),pre_grip(:));
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'n = %d pre, %d post\n',sum(~isnan(pre_grip(:))),sum(~isnan(post_grip(:))));
        fprintf(fid,'df = %d\n', stats.df);
        fprintf(fid,'mean_pre = %f\n', nanmean(pre_grip(:)));
        fprintf(fid,'mean_cast = %f\n', nanmean(post_grip(:)));
        fprintf(fid,'percent_change = %f\n', (nanmean(post_grip(:))-nanmean(pre_grip(:)))/nanmean(pre_grip(:))*100);
        fprintf(fid,'t = %f\n', stats.tstat);
        fprintf(fid,'P = %f\n', p);
    end

    % Left UE
    fprintf(fid,'%s\n','grip strength t-tests: left UE');
    for i=1:length(subjects)
        subject = subjects{i};
        load([strength_dir subject '_grip_left.mat'],'grip','pre','post');
        pre_grip=grip(pre,:);
        post_grip=grip(post,:);
        [~,p,~,stats]=ttest2(post_grip(:),pre_grip(:));
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'n = %d pre, %d post\n',sum(~isnan(pre_grip(:))),sum(~isnan(post_grip(:))));
        fprintf(fid,'df = %d\n', stats.df);
        fprintf(fid,'mean_pre = %f\n', nanmean(pre_grip(:)));
        fprintf(fid,'mean_cast = %f\n', nanmean(post_grip(:)));
        fprintf(fid,'percent_change = %f\n', (nanmean(post_grip(:))-nanmean(pre_grip(:)))/nanmean(pre_grip(:))*100);
        fprintf(fid,'t = %f\n', stats.tstat);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% t-test of fine motor skill immediately after cast removal vs. pre
if pegboard_ttest
    fid=fopen([stats_outdir 'pegboard_ttest.txt'],'w');
    % Right UE
    fprintf(fid,'%s\n','Purdue pegboard t-tests: right UE');
    for i=1:length(subjects)
        subject = subjects{i};
        load([pegboard_dir subject '_pegs_right.mat'],'pegs','pre','post');
        pre_grip=pegs(pre,:);
        post_grip=pegs(post,:);
        [~,p,~,stats]=ttest2(post_grip(:),pre_grip(:));
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'n = %d pre, %d post\n',sum(~isnan(pre_grip(:))),sum(~isnan(post_grip(:))));
        fprintf(fid,'df = %d\n', stats.df);
        fprintf(fid,'mean_pre = %f\n', nanmean(pre_grip(:)));
        fprintf(fid,'mean_cast = %f\n', nanmean(post_grip(:)));
        fprintf(fid,'percent_change = %f\n', (nanmean(post_grip(:))-nanmean(pre_grip(:)))/nanmean(pre_grip(:))*100);
        fprintf(fid,'t = %f\n', stats.tstat);
        fprintf(fid,'P = %f\n', p);
    end

    % Left UE
    fprintf(fid,'%s\n','Purdue pegboard t-tests: left UE');
    for i=1:length(subjects)
        subject = subjects{i};
        load([pegboard_dir subject '_pegs_left.mat'],'pegs','pre','post');
        pre_grip=pegs(pre,:);
        post_grip=pegs(post,:);
        [~,p,~,stats]=ttest2(post_grip(:),pre_grip(:));
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'n = %d pre, %d post\n',sum(~isnan(pre_grip(:))),sum(~isnan(post_grip(:))));
        fprintf(fid,'df = %d\n', stats.df);
        fprintf(fid,'mean_pre = %f\n', nanmean(pre_grip(:)));
        fprintf(fid,'mean_cast = %f\n', nanmean(post_grip(:)));
        fprintf(fid,'percent_change = %f\n', (nanmean(post_grip(:))-nanmean(pre_grip(:)))/nanmean(pre_grip(:))*100);
        fprintf(fid,'t = %f\n', stats.tstat);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% permutation test of ?r based on exponential decay model
if fc_timecourse_perm
    fid=fopen([stats_outdir 'fc_timecourse_perm.txt'],'w');
    % upper extremity
    fprintf(fid,'%s\n','fc time course permutation test: L-SM1(ue) to R-SM1(ue)');
    for i=1:length(subjects)
        subject = subjects{i};
        load([settings_dir subject '_settings.mat'])
        numperm = 1000;
        % Upper extremity
        roi1=1+6; roi2=2+6;
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
        data=squeeze(roi_corrmat(roi1,roi2,:));
        duration=length(data)*24;

        % fit exponential decay model model
        ObjectiveFunction = @(x)fit_plasticity_model_2alpha(data,duration, cast_onset, cast_offset, measurement_times, x);
        initial_params=[0 0.005 0.005];
        params_estimate=lsqnonlin(ObjectiveFunction,initial_params);
        r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
        delta_r = r(max(measurement_times(measurement_times<cast_offset)))-mean(r(1:cast_onset));

        % permute
        null_delta_r=zeros(1,numperm);
        for i=1:numperm
            permdata=data(randperm(length(data)));
            ObjectiveFunction = @(x)fit_plasticity_model_2alpha(permdata,duration, cast_onset, cast_offset, measurement_times, x);
            r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
            null_delta_r(i) = r(max(measurement_times(measurement_times<cast_offset)))-mean(r(1:cast_onset));
        end
        p = sum(abs(null_delta_r)>abs(delta_r))/numperm;
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'delta_r = %f\n', delta_r);
        fprintf(fid,'P = %f\n', p);
    end
    % lower extremity
    fprintf(fid,'\n%s\n','fc time course permutation test: L-SM1(le) to R-SM1(le)');
    for i=1:length(subjects)
        subject = subjects{i};
        load([settings_dir subject '_settings.mat'])
        numperm = 1000;
        roi1=1+12; roi2=2+12;
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
        data=squeeze(roi_corrmat(roi1,roi2,:));
        duration=length(data)*24;

        % fit exponential decay model model
        ObjectiveFunction = @(x)fit_plasticity_model_2alpha(data,duration, cast_onset, cast_offset, measurement_times, x);
        initial_params=[0 0.005 0.005];
        params_estimate=lsqnonlin(ObjectiveFunction,initial_params);
        r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
        delta_r = r(max(measurement_times(measurement_times<cast_offset)))-mean(r(1:cast_onset));

        % permute
        null_delta_r=zeros(1,numperm);
        for i=1:numperm
            permdata=data(randperm(length(data)));
            ObjectiveFunction = @(x)fit_plasticity_model_2alpha(permdata,duration, cast_onset, cast_offset, measurement_times, x);
            r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
            null_delta_r(i) = r(max(measurement_times(measurement_times<cast_offset)))-mean(r(1:cast_onset));
        end
        p = sum(abs(null_delta_r)>abs(delta_r))/numperm;
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'delta_r = %f\n', delta_r);
        fprintf(fid,'P = %f\n', p);
    end
    % face
    fprintf(fid,'\n%s\n','fc time course permutation test: L-SM1(f) to R-SM1(f)');
    for i=1:length(subjects)
        subject = subjects{i};
        load([settings_dir subject '_settings.mat'])
        numperm = 1000;
        roi1=1; roi2=2;
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat');
        data=squeeze(roi_corrmat(roi1,roi2,:));
        duration=length(data)*24;

        % fit exponential decay model model
        ObjectiveFunction = @(x)fit_plasticity_model_2alpha(data,duration, cast_onset, cast_offset, measurement_times, x);
        initial_params=[0 0.005 0.005];
        params_estimate=lsqnonlin(ObjectiveFunction,initial_params);
        r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
        delta_r = r(max(measurement_times(measurement_times<cast_offset)))-mean(r(1:cast_onset));

        % permute
        null_delta_r=zeros(1,numperm);
        for j=1:numperm
            permdata=data(randperm(length(data)));
            ObjectiveFunction = @(x)fit_plasticity_model_2alpha(permdata,duration, cast_onset, cast_offset, measurement_times, x);
            r = plasticity_model_2alpha(data,duration, cast_onset, cast_offset, params_estimate);
            null_delta_r(j) = r(max(measurement_times(measurement_times<cast_offset)))-mean(r(1:cast_onset));
        end
        p = sum(abs(null_delta_r)>abs(delta_r))/numperm;
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'delta_r = %f\n', delta_r);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% one-sample ttest to determine how quickly FC changed during casting
if cast_onset_ttest
    fid=fopen([stats_outdir 'one_sample_FC_ttest.txt'],'w');
    fprintf(fid,'%s\n','One-sample t-tests of SM1 FC');
    for i=1:length(subjects)
        subject = subjects{i};
        fprintf(fid,'\n%s\n',subject);
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat')
        
        roi1=1+6;
        roi2=2+6;
        data=squeeze(roi_corrmat(roi1,roi2,:));
        for j=1:length(cast_scannums)
            [~,p]=ttest2(data(cast_scannums(j)), data(pre_scannums));
            if p<0.05
                break
            end
        end
        
        fprintf(fid,'cast day %d\n', j);
        delta_r = data(cast_scannums(j)) - nanmean(data(pre_scannums));
        fprintf(fid,'delta r = %f\n', delta_r);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% permutation test of modularity change during casting
if modularity_perm
    fid=fopen([stats_outdir 'modularity_perm.txt'],'w');
    % cortical parcels
    fprintf(fid,'%s\n','modularity permutation test: cortical SMN');
    for i=1:length(subjects)
        subject = subjects{i};
        load([settings_dir subject '_settings.mat'])
        numperm = 10000;
        load([parcel_outdir subject '_parcel_corrmat.mat'],'parcel_corrmat','communities');
        parcels=ft_read_cifti_mod(parcel_path);
        parcel_list=unique(parcels.data(parcels.data>0));
        struct=parcels.brainstructure(parcels.brainstructure>0);
        assert(length(parcels.data)==length(struct));
        hems=zeros(size(parcel_list));
        for i=1:length(parcel_list)
            hems(i)=mode(struct(parcels.data==parcel_list(i)));
        end
        if (sort_parcels)
            [~,sorti]=sort(hems);
            parcel_list=parcel_list(sorti);
        end
        motor_parcels=ft_read_cifti_mod([parcel_outdir subject '_primary_somatomotor_parcels.dtseries.nii']);
        right_hand_nodes=find(ismember(parcel_list,unique(parcels.data(motor_parcels.data==1))));
        other_somatomotor_nodes=find(ismember(parcel_list,unique(parcels.data(ismember(motor_parcels.data,2:6)))));
        motor_nodes=[right_hand_nodes; other_somatomotor_nodes];
        pre_graph=mean(parcel_corrmat(motor_nodes,motor_nodes,pre_scannums),3);
        cast_graph=mean(parcel_corrmat(motor_nodes,motor_nodes,cast_scannums),3);
        communities=ones(size(motor_nodes));
        communities(1:length(right_hand_nodes))=2;
        pre_modularity=modularity(pre_graph,communities);
        cast_modularity=modularity(cast_graph,communities);
        diff=cast_modularity-pre_modularity;
        perc_change=diff/pre_modularity*100;

        % perm
        scannums=[pre_scannums cast_scannums];
        null_diff=zeros(1,numperm);
        for j=1:numperm
            perm_scannums=scannums(randperm(length(scannums)));
            pre_perm=perm_scannums(1:length(pre_scannums));
            cast_perm=perm_scannums((length(pre_scannums)+1):end);
            pre_graph=mean(parcel_corrmat(motor_nodes,motor_nodes,pre_perm),3);
            cast_graph=mean(parcel_corrmat(motor_nodes,motor_nodes,cast_perm),3);
            pre_modularity=modularity(pre_graph,communities);
            cast_modularity=modularity(cast_graph,communities);
            null_diff(j)=cast_modularity-pre_modularity;
        end
        p = sum(abs(null_diff)>abs(diff))/numperm;
        fprintf(fid,'\n%s\n',subject);
        fprintf(fid,'percent_change = %f\n', perc_change);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% t-test comparing ALFF Cast vs. Pre
if alff_ttest
    fid=fopen([stats_outdir 'alff_ttest.txt'],'w');
    fprintf(fid,'%s\n','ALFF t-test: Cast vs. Pre');
    for i=1:length(subjects)
        subject = subjects{i};
        fprintf(fid,'\n%s\n',subject);
        load([settings_dir subject '_settings.mat'])
        load ([roi_outdir subject '_ROI_timecourses.mat'],'roi_time')
        
        % L-SM1ue
        roi_num=1+6;
        [~,~,change,p] = plot_roi_spec (roi_time,roi_num,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
        fprintf(fid,'L-SM1ue');
        fprintf(fid,'percent_change = %f\n', change);
        fprintf(fid,'P = %f\n', p);
        
        % R-SM1ue
        roi_num=2+6;
        [~,~,change,p] = plot_roi_spec (roi_time,roi_num,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
        fprintf(fid,'R-SM1ue');
        fprintf(fid,'percent_change = %f\n', change);
        fprintf(fid,'P = %f\n', p);
        
        % L-SM1le
        roi_num=1+12;
        [~,~,change,p] = plot_roi_spec (roi_time,roi_num,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
        fprintf(fid,'L-SM1le');
        fprintf(fid,'percent_change = %f\n', change);
        fprintf(fid,'P = %f\n', p);
        
        % L-SM1face
        roi_num=1;
        [~,~,change,p] = plot_roi_spec (roi_time,roi_num,pre_scannums,cast_scannums,post_scannums,cast_color,rest_frames,TR);
        fprintf(fid,'L-SM1face');
        fprintf(fid,'percent_change = %f\n', change);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% t-test comparing # pulses detected Pre vs. Cast
if pulse_count_ttest
    fid=fopen([stats_outdir 'pulse_count_ttest.txt'],'w');
    fprintf(fid,'%s\n','Pulse count t-test: Cast vs. Pre');
    for i=1:length(subjects)
        subject = subjects{i};
        fprintf(fid,'\n%s\n',subject);
        load([settings_dir subject '_settings.mat'])
        load([detection_outdir subject '_pulse_list.mat'],'pulse_count')
        
        [~,p]=ttest2(pulse_count(cast_scannums), pulse_count(pre_scannums));
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% Wilcoxon Signed Rank test comparing pulse times in SMA, SM1 + Cblm
if pulse_time_WSR_test
    fid=fopen([stats_outdir 'pulse_propagation_tests.txt'],'w');
    fprintf(fid,'%s\n','Pulse propagation wilcoxon signed rank test');
    for i=1:length(subjects)
        subject = subjects{i};
        fprintf(fid,'\n%s\n',subject);
        load([tmask_outdir subject '_tmask.mat'],'tmask')
        load([detection_outdir subj '_pulse_list.mat'],'pulse_list','pulse_count')
        load([roi_outdir subj '_ROI_timecourses.mat'],'roi_time');
 
        roi1=1; % L-SM1 (1 = L-SM1, 2 = R-SM1, 3/4 = L/R-SMA, 5/6 = L/R-Cblm)
        roi1=roi1+6; % Hand (0 = Tongue, 6 = Hand, 12 = Foot)
        rois=[3 1 6]+6;
        roi_names={'SMA','SM1','Cblm'};
        reference_roi=2; % which ROI in "rois" should be used as a zero point for pulse timing
        plot_timecourses=false;
        plot_time_delays=false;
        
        diary on
        pulse_propagation(tmask, pulse_list, pulse_count, roi_time, TR, subject, plot_timecourses, rois, roi_names, reference_roi, plot_time_delays)
        diary off
        diary([stats_outdir 'pulse_propagation_tests.txt'])
    end
    fclose(fid)
end

% t-test comparing SM1 and Cblm FC during control experiment
if onoff_fc_ttest
    fid=fopen([stats_outdir 'onoff_fc_ttests.txt'],'w');
    fprintf(fid,'%s\n','FC t-tests: On vs. Off');
    for i=1:length(subjects)
        subject = subjects{i};
        fprintf(fid,'\n%s\n',subject);
        load([roi_outdir subject '_ROI_corrmat.mat'],'roi_corrmat')
        
        % SM1
        fprintf(fid,'%s\n','L-SM1ue : R-SM1ue');
        roi1=1+6;
        roi2=2+6;
        data=squeeze(roi_corrmat(roi1,roi2,:));
        delta_r = nanmean(data(on_scannums)) - nanmean(data(off_scannums));
        [~,p]=ttest2(data(on_scannums), data(off_scannums));
        fprintf(fid,'delta r = %f\n', delta_r);
        fprintf(fid,'P = %f\n', p);
        
        % Cblm
        fprintf(fid,'%s\n','R-Cblmue : L-Cblmue');
        roi1=5+6;
        roi2=6+6;
        data=squeeze(roi_corrmat(roi1,roi2,:));
        delta_r = nanmean(data(on_scannums)) - nanmean(data(off_scannums));
        [~,p]=ttest2(data(on_scannums), data(off_scannums));
        fprintf(fid,'delta r = %f\n', delta_r);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end

% t-test comparing # pulses detected during control experiment
if onoff_pulse_ttest
    fid=fopen([stats_outdir 'onoff_pulse_count_ttests.txt'],'w');
    fprintf(fid,'%s\n','Pulse count t-tests: On vs. Off');
    for i=1:length(subjects)
        subject = subjects{i};
        fprintf(fid,'\n%s\n',subject);
        load([detection_outdir subject '_pulse_list.mat'],'pulse_count')
        
        perc_change = nanmean(pulse_count(on_scannums))/nanmean(pulse_count(off_scannums))-1;
        [~,p]=ttest2(pulse_count(on_scannums), pulse_count(off_scannums));
        fprintf(fid,'percent change = %f\n', perc_change);
        fprintf(fid,'P = %f\n', p);
    end
    fclose(fid)
end
